# Introduction to solid state quantum chemistry calculations

This project is a gitlab page that provides a protocol to implement 
of solid state quantum chemistry calculations using the 
VASP simulation package.

The web side is available here: [gvallverdu.gitlab.io/rfct-tp-solide/](https://gvallverdu.gitlab.io/rfct-tp-solide/ )

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gvallverdu.gitlab.io/rfct-tp-solide/">RFCT Solid State Practical Session</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gsalvatovallverdu.gitlab.io">Germain Salvato Vallverdu</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<br/><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

## Requirements

- [Sphinx][]
- [sphinx_rtd_theme][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. [Install][sphinx] Sphinx
3. move to `src/` directory: `cd src/`
4. Generate the documentation: `make`

The generated HTML will be located in the location specified by `conf.py`,
in this case `src/_build/html`.


[sphinx_rtd_theme]: https://sphinx-rtd-theme.readthedocs.io/en/stable/index.html
[sphinx]: http://www.sphinx-doc.org/
