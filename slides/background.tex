\section[Background]{Chimie théorique du solide de base}

\subsection{DFT}

\begin{frame}
    \frametitle{DFT : \textit{Density Functional Theory} -- Idées générales}

    \begin{block}{La DFT est basé sur la densité électronique $\rho(\vr)$}
    \begin{equation*}
        \rho(\vr) = \int_1\ldots\int_{N-1} |\Psi(\vr_1,\ldots,\vr_N)|^2d\vr_1\ldots d\vr_{N-1}
    \end{equation*}
    %
    L'énergie est exprimée comme une fonctionnelle de la densité :
    %
    \begin{equation*}
        E[\rho] = E_o[\rho] + J[\rho] + V_{xc}[\rho]
    \end{equation*}
    \vspace{-\baselineskip}
    \begin{itemize}
        \item $E_o[\rho]$ : énergie monoélectronique
        \item $J[\rho]$ : énergie électrostatique de coulomb
        \item $V_{xc}[\rho]$ : énergie d'échange et corrélation => fonction inconnue
    \end{itemize}
    \end{block}

    \begin{exampleblock}{Choix de la fonctionnelle de la densité}
        En DFT, le choix du niveau d'approximation se fait en choisissant une fonctionnelle.

        \smallskip

        Différents types d'approximations :
        \begin{itemize}
            \item LDA : \textit{Local Density Approximation} $V_{xc}[\rho](x_o) = f[\rho(x_o)]$
            \item GGA : \textit{Generalised Gradient Approximation} $V_{xc}[\rho](x_o) = f[\rho(x_o),\nabla_{x_o}\rho]$
            \item meta-GGA : $V_{xc}[\rho](x_o) = f[\rho(x_o),\nabla_{x_o}\rho, \Delta_{x_o}\rho]$
            \item hybride : GGA avec un peu d'échange Hartree-Fock (exact)
        \end{itemize}
    \end{exampleblock}

\end{frame}

\subsection[PW basis]{Bases d'onde planes}


\begin{frame}
    \frametitle{Bases d'onde planes}

    \begin{block}{Fonction d'onde adaptée à la symétrie de translation}
    \begin{itemize}
        \item D'après le théorème de Block les fonctions d'onde adaptées à la symétrie de translation s'écrivent
        {\small
        \begin{equation*}
            \Psi_{j,\vk}(\vr) = \sum_{m=1}^{N_{maille}} \phi_j(\vr_m)\exp\left(i\,\vk\cdot\vr_m\right)
        \end{equation*}}
        \item $\vk$ : caractérise la symétrie de translation.
        \item $\lbrace\phi_j\rbrace$ : Bases utilisées pour décrire la fonction d'onde du système. Deux grandes familles :
        \begin{itemize}
            \item Orbitales atomiques : $\phi_j(\vr) = \PsiH$ (Gaussian Type Orbital)
            \item Ondes planes $\phi_j(\vr) = \exp\left(i\,\vec{K}.\vr\right)$ où $\vec{K}$ est un vecteur d'onde.
        \end{itemize}
    \end{itemize}
    \end{block}

    \vfill

    \begin{minipage}[c]{.6\textwidth}
         Dans une base d'ondes planes :
         \begin{equation*}
            \Psi_{\vk}(\vr) = \sum_{m=1}^{N_{maille}} \exp\left[i\,(\vk+\vec{K})\cdot\vr_m\right]
        \end{equation*}
        La taille de la base est fixée par la valeur maximale de l'énergie $\eps$ d'une onde plane (\textit{cut-off})
        \begin{equation*}
            \eps = \hbar^2|\vk+\vec{K}|^2 \, / \, 2m_e
        \end{equation*}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{.34\textwidth}
	    \begin{exampleblock}{Avantages}
	        \begin{itemize}
	            \item Adaptées à la symétrie cristalline.
	            \item Un seul paramètre : l'énergie de \textit{cut-off}
	        \end{itemize}
	    \end{exampleblock}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Pseudo-potentiels - position du problème}

    \begin{center}
        \begin{minipage}{.52\textwidth}
        \begin{tikzpicture}
            \node at (0,0) {\includegraphics[scale=.25]{OA4s}};
            \node at (.6,0.3) {$\psi$};
            \node at (-1.3,0.3) {$\tilde{\psi}$};
        \end{tikzpicture}
        \end{minipage}
        \hfill
        \begin{minipage}{.43\textwidth}
            \begin{block}{Problématique}
                \begin{itemize}
                    \item Proche du noyau la fonction d'onde oscille rapidement
                    \item Loin du noyau elle est monotone
                \end{itemize}

                \centering => Difficultés pour décrire simultanément les deux régions.
            \end{block}
        \end{minipage}
    \end{center}

    \begin{exampleblock}{Solution}
        \begin{itemize}
            \item Calcul tout électrons avec une grande base.
            \item Pseudo-potentiel : Proche du noyau, la fonction d'onde $\psi$ est remplacée par une pseudo-fonction $\tilde{\psi}$ qui oscille moins. Approche du coeur gelé (\textit{frozen core}).
        \end{itemize}
    \end{exampleblock}

    \begin{exampleblock}{Avantage}
        \begin{itemize}
            \item Permet de diminuer la taille de la base (le \textit{cutoff}) et le temps de calcul.
        \end{itemize}
    \end{exampleblock}

\end{frame}

\begin{frame}
    \frametitle{Pseudo-potentiels - \textit{Projector Augmented Wave}}

     \begin{center}
        \begin{minipage}{.51\textwidth}
        \begin{tikzpicture}
            \node at (0,0) {\includegraphics[scale=.25]{OA4s_paw}};
            \node at (.6,0.3) {$\psi$};
            \node at (-1.3,0.3) {$\tilde{\psi}$};
        \end{tikzpicture}
        \end{minipage}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{itemize}
                \item L'équation de Schrödinger est résolue en présence du pseudo-potentiel.  \\

                \item Dans une sphère centrée sur chaque atome la fonction d'onde est rajoutée \textit{a posteriori}.
            \end{itemize}
        \end{minipage}
    \end{center}

    \centering
    \begin{tikzpicture}[scale=.8]
        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, pattern={north east lines}]
        \draw (0,0) rectangle (3,3);
        \node[atom] at (3/4,3/4) {};
        \node[atom] at (3/4,9/4) {};
        \node[atom] at (9/4,3/4) {};
        \node[atom] at (9/4,9/4) {};
        \node[above] at (1.5,3) {Tout électrons};

        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, dashed]
        \draw (4,0) rectangle (7,3);
        \node[atom] at (4+3/4,3/4) {};
        \node[atom] at (4+3/4,9/4) {};
        \node[atom] at (4+9/4,3/4) {};
        \node[atom] at (4+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (5.5,3) {pseudo fonction};

        \tikzstyle{atom} = [circle, draw, minimum width = 6mm]
        \draw[dashed] (8,0) rectangle (11,3);
        \node[atom] at (8+3/4,3/4) {};
        \node[atom] at (8+3/4,9/4) {};
        \node[atom] at (8+9/4,3/4) {};
        \node[atom] at (8+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (9.5,3) {pseudo fonction\\ dans la sphère};

        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, pattern={north east lines}]
        \draw[dashed] (12,0) rectangle (15,3);
        \node[atom] at (12+3/4,3/4) {};
        \node[atom] at (12+3/4,9/4) {};
        \node[atom] at (12+9/4,3/4) {};
        \node[atom] at (12+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (13.5,3) {Tout électrons dans la sphère};

        \node at (3.5,1.5) {\Large =};
        \node at (7.5,1.5) {\Large-};
        \node at (11.5,1.5) {\Large+};

        \node at (5.75,1.5) (s1) {};
        \node at (8.75,.75) (s2) {};
        \node at (12.75,.75) (s3) {};
    \end{tikzpicture}

    \begin{equation*}
        \left\vert\psi\right\rangle
            = \tikz{\node (pseudo) {};}\hspace{-1.5ex}
            \left\vert\,\tilde{\psi}\, \right\rangle
            + \sum_i \left(\left\vert \phi_i \right\rangle
              \tikz{\node (pseudo in sphere) {};}\hspace{-1.5ex}
            - \tikz{\node (ae in sphere) {};}\hspace{-1.5ex}
            \left\vert \tilde{\phi}_i \right\rangle
            \right)
            \left\langle\tilde{\textrm{p}}_i\left\vert\tilde{\psi}\right\rangle\right.
    \end{equation*}

    \begin{tikzpicture}[overlay, thick, -stealth]
        \draw ($(pseudo) + (.2,.25)$) to[bend left] (s1);
        \draw ($(pseudo in sphere) + (-.35,.25)$) to[bend left] (s2.center);
        \draw ($(ae in sphere) + (0.1,.25)$) to[out=50, in=230] (s3.center);
    \end{tikzpicture}
\end{frame}

\begin{frame}
    \frametitle{Conditions périodiques aux limites}

    \uneQuestion{Comment modéliser un système infini périodique ?}

    \begin{center}
         La motif élémentaire est répété périodiquement dans toutes les directions de l'espace.

	    \begin{tikzpicture}[scale=.5]
	        \tikzstyle{atom} = [ball color = bleuP, circle, scale = 0.5]
		    % dessin de la boite initiale
		    \node[below] at (0,3) {motif};
			\path[draw,very thick] (-1,3) rectangle (1,5);

			\def\x{-1}
			\def\y{3}
	    	\node[atom] at (\x+0.45,\y+0.3) {};
			\node[atom] at (\x+1.6 ,\y+1.4) {};
			\node[atom] at (\x+0.5 ,\y+1.45) {};
			\node[atom] at (\x+1.1 ,\y+0.7) {};

			\foreach \x in {6,8,10} {
				\foreach \y in {1,3,5} {
					% placement molécules d'eau d'une copie
					\node[atom] at (\x+0.45,\y+0.3) {};
					\node[atom] at (\x+1.6 ,\y+1.4) {};
					\node[atom] at (\x+0.5 ,\y+1.45) {};
					\node[atom] at (\x+1.1 ,\y+0.7) {};

					% dessin de la boite d'une copie
					\path[draw] (\x,\y) rectangle (\x+2,\y+2);
				}
			}

			% boite centrale plus eppaisse
			\path[draw,very thick] (8,3) rectangle (10,5);

			% la flèche
			\path[draw, thick,-stealth] (2,4) -- (4,4);

			% trait en pointillé (en premier pour être dessous
			\foreach \x in {6,8,10,12} {
				% verticaux
				\path[draw,dashed] (\x,0) -- (\x,1) (\x,7) -- (\x,8);
				% horizontaux
				\path[draw,dashed] (5,\x-5) -- (6,\x-5) (12,\x-5) -- (13,\x-5);
			}

            \node at (1,7) (img) {Images périodiques};
            \draw[thick, -stealth] (img.east) to[out=0] (6,5.8);
		\end{tikzpicture}
    \end{center}

        \begin{minipage}[c]{.48\textwidth}
            \structure{Avantages}
                \begin{itemize}
                    \item Idéal pour représenter un système périodique infini (cristal parfait).
                    \item Bien adapté à l'utilisation des bases d'ondes planes
                \end{itemize}
        \end{minipage}
        \hfill
        \begin{minipage}[c]{.48\textwidth}
            \structure{Inconvéniants}
                \begin{itemize}
                    \item Traitement des systèmes 2D, 1D ou non périodiques
                    \item Traitement des défauts, occupations partielles
                \end{itemize}
        \end{minipage}
\end{frame}
