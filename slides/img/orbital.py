#!/usr/bin/python
# -*- coding=utf-8 -*-

import scipy as sp
from scipy import sqrt, exp

ao = 0.529

def OA2s(r, z = 1.):
    return (z/ao)**(3/2) * 1./(2.*sqrt(2.)) * (2. - z*r / ao) * exp(-z*r / (2.*ao))

def OA2p(r, z = 1.):
    return (z/ao)**(3/2) * 1./(2.*sqrt(6.)) * z*r / ao  * exp(-z*r / (2.*ao))

def OA3s(r, z = 1.):
    poly = 6. - 4. * z * r / ao + 4 * z**2 * r**2 / (9. * ao**2)
    return (z/ao)**(3/2) * 1./(9. * sqrt(3.)) * poly * exp(-z*r / (3.*ao))

def OA4s(r, z = 1.):
    poly = 24. - 18. * z * r / ao + 3. * z**2 * r**2 / ao**2 - z**3 * r**3 / (8. * ao**3)
    return (z/ao)**(3/2) * 1. / 96. * poly * exp(-z*r / (4.*ao))

r = sp.linspace(0., 30., 1000)

import matplotlib.pyplot as plt
#plt.plot(r, [ri**2 * OA4s(ri)**2 for ri in r], "r-")
plt.plot(r, [OA4s(ri) for ri in r], "b-", r, [0 for ri in r])
#plt.plot(r, [OA3s(ri) for ri in r], "b-")
plt.show()
exit(0)

from data2file import d2f
data = zip(r, [ri**2 * OA4s(ri)**2 for ri in r])
d2f(data)
