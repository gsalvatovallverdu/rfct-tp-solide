%\subsection[Plane waves]{Plane waves basis set and pseudopotential approach}
\subsection[Basis Sets]{Basis sets and pseudopotential approach}

\begin{frame}
    \frametitle{Density functional Theory and basis sets}
    \framesubtitle{Kohn-Sham equations}
    
    \structure{Nonlinear eigenvalue equations}
    
    \begin{align*}
        \left(-\frac{1}{2}\Delta + V_{ion} + J[n] + V_{xc}[n]\right) \Psi_i & = \eps_i \Psi_i \\
        n(r) & = \sum_{i=1}^N \left\vert \Psi \right\vert^2
    \end{align*}

    \structure{Resolution}
    
    \begin{itemize}
        \item Require self-consistent solution
        \item In order to solve these equations we need to expand the wavefunction
        $\Psi$ in a basis set
        \begin{equation*}
            \Psi = \sum_j c_j \varphi_j
        \end{equation*}
    \end{itemize}

    \structure{Basis set}
    
    \begin{itemize}
        \item This family has to be finite, limited in size
        \item It maps a continuous problem to linear algebra:
        \begin{itemize}
            \item Wavefunctions are vectors
            \item Operators are matrices
        \end{itemize}
        \medskip
        \item The basis set should allow enough flexibility to represent the "real" wavefunctions
    \end{itemize}

\end{frame}

\subsubsection[Atomic orbital]{Atomic orbital basis set (LCAO)}

\begin{frame}
    \frametitle{Atomic basis set}
    \framesubtitle{The chemical world}
    
    \structure{Characteristics}
    
    \begin{itemize}
        \item Also called "localized basis sets": centered on nuclei (or sometimes ghost atoms)
        \item Slater-types or Gaussian functions
        \item Matches the "linear combination of atomic orbitals" (LCAO) approach    
    \end{itemize}
    
    \pause
    \medskip
    \structure{How to choose it ?}

    \begin{itemize}
        \item Function type : Slater type, GTO (gaussian type orbital) ...
        \item Number of orbitals per atom
        \item Contraction (s, p, sp ...)
        \item Including polarisation/diffuse orbitals (and how many)?
    \end{itemize}
    
    \pause
    \medskip
    \structure{Example}
    
    \begin{itemize}
        \item Pople Basis Set : 3-21G, 6-31G, 6-311G++**
        \item Correlation consistent : cc-pVTZ, cc-pVQZ
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Atomic basis set}
    \framesubtitle{Pros and cons}
    
    \begin{block}{Pros}
        \begin{itemize}
            \item Correspond to chemical picture
            \item Describe well the atoms, even with few orbitals
            \item No so much harder to describe inner electrons
        \end{itemize}
    \end{block}

    \pause
    \begin{exampleblock}{Intermediate}
        \begin{itemize}
            \item Easily tunable
            \item No implicit periodicity
        \end{itemize}
    \end{exampleblock}

    \pause
    \begin{alertblock}{Cons}
        \begin{itemize}
            \item Non-orthogonal
            \item Depend on atomic positions, Pulay stress
            \item Basis set superposition error (BSSE)
            \item So many parameters... how do you optimize ? Transferability from
            molecule to solid state ? 
        \end{itemize}
    \end{alertblock}
\end{frame}

% ------------------------------------------------------------------------------
\subsubsection[Periodic conditions]{Periodic boundary conditions}

\begin{frame}
    \frametitle{Periodic boundary conditions}
    \framesubtitle{Condensed matter}
    
    \begin{center}
        \includegraphics[width=.6\textwidth]{periodic_bound}
    \end{center}
    
    \begin{itemize}
        \item The system is replicated in all direction to mimic condensed matter
        \item Ideal crystal : no defaults, no side effects
        \item Image size issues
    \end{itemize}

\end{frame}

% ------------------------------------------------------------------------------
\subsubsection[Plane waves]{Plane wave basis set approach}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{What about plane waves and condensed matter ?}

    \structure{Did you say plane waves ?}
    
    \begin{center}
        \includegraphics[height=6cm]{wave}
    \end{center}

\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{What about plane waves and condensed matter ?}

    \structure{Did you say plane waves ?}

    \begin{center}
        \includegraphics[height=6cm]{planewaves}
    \end{center}

    \begin{equation*}
        \varphi_{\alpha}(\vec{r}) = \frac{1}{\sqrt{\Omega}} \exp \left( i \vec{G}_{\alpha}\cdot\vec{r}\right)
    \end{equation*}
    %	    
    \begin{itemize}
        \item $\Omega$ is the volume   
        \item $\vec{G}_{\alpha}$ is a vector of the reciprocal lattice
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{Basis set construction}

    \structure{Kinetic energy associated with each plane wave}
    
    \begin{align*}
        \hat{T}\varphi_{\alpha}(\vec{r}) & = -\frac{1}{2}\Delta \frac{1}{\sqrt{\Omega}} \exp \left( i \vec{G}_{\alpha}\cdot\vec{r}\right) \\
            & = \frac{\vert\vert\vec{G}^2_{\alpha}\vert\vert}{2}
    \end{align*}

    We define \textit{cut-off} energy corresponding to $\vec{G}_{max}$.
    \begin{equation*}
        E_{cut-off} = \frac{\vert\vert\vec{G}^2_{max}\vert\vert}{2}
    \end{equation*}
    
    \pause
    \structure{In one dimension}
    
    \begin{align*}
        \vec{a} & = a \vec{i} & 
        \vec{a}^* & = \frac{2\pi}{a} \vec{i} &
        \vec{G} & = u \vec{a}^* = \frac{2\pi u}{a} \vec{i} &
        \varphi_u & = \frac{1}{a} \exp\left(\frac{2 i \pi u}{a} x\right) &
                u & \in \mathbb{Z}
    \end{align*}

    \begin{itemize}
        \item $u$ is increased till $\vec{G}_{max} = \frac{2\pi u_{max}}{a}$
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{Example with a 3s atomic orbital}

    Consider the radial part of a 3s atomic orbital:
    \begin{equation*}
        \varphi_{3,0,0}(r) = 
        \frac{1}{9\sqrt{3}} \left(\frac{Z}{a_o}\right)^{3/2} 
        \left(6 - \frac{4 Z r}{a_o} + \frac{4 Z^2 r^2}{9 a_o^2}\right) 
        \exp\left(-\frac{Z r}{4 a_o}\right)
    \end{equation*}
    
    \begin{center}%
        \only<1>{\includegraphics[width=.8\textwidth]{fit_AO3s_1}}%
        \only<2>{\includegraphics[width=.8\textwidth]{fit_AO3s_5}}%
        \only<3>{\includegraphics[width=.8\textwidth]{fit_AO3s_10}}%
        \only<4>{\includegraphics[width=.8\textwidth]{fit_AO3s_20}}%
        \only<5>{\includegraphics[width=.8\textwidth]{fit_AO3s_40}}%
        \only<6>{\includegraphics[width=.8\textwidth]{fit_AO3s_50}}%
    \end{center}
    
    \pause[5]
    \vspace*{-5mm}
    \begin{center}
    40 plane waves are necessary to obtain RMS less than 1.
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{Pros and cons}

    \begin{block}{Pros}
        \begin{itemize}
            \item Orthogonal
            \item Independent of atomic positions (no Pulay forces)
            \item Improving is easy: increase the cutoff
            \item Easy to use on any atomic type (no basis set optimization)
            \item Use of Fast Fourier Transform
        \end{itemize}
    \end{block}

    \pause
    \begin{exampleblock}{Intermediate}
        \begin{itemize}
            \item Implicit periodicity
            \item All-or-nothing description (no spot favored)
        \end{itemize}
    \end{exampleblock}

    \pause
    \begin{alertblock}{Cons}
        \begin{itemize}
            \item Large number of basis functions needed
            \item How do you get back chemical information?
            \item Inner wavefunctions vary too rapidly: Pseudopotentials are needed
        \end{itemize}
    \end{alertblock}
\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set and Bloch Orbitals}
    \framesubtitle{Symmetry adapted basis set}

    \begin{block}{Wave functions adapted to the translational symmetry}
    \begin{itemize}
        \item D'après le théorème de Bloch les fonctions d'onde adaptées à la symétrie de translation s'écrivent
        {\small
        \begin{equation*}
            \Psi_{j,\vk}(\vr) = \sum_{m=1}^{N_{maille}} \phi_j(\vr_m)\exp\left(i\,\vk\cdot\vr_m\right)
        \end{equation*}}
        \item $\vk$ : caractérise la symétrie de translation.
        \item $\lbrace\phi_j\rbrace$ : Bases utilisées pour décrire la fonction d'onde du système. Deux grandes familles :
        \begin{itemize}
            \item Orbitales atomiques : $\phi_j(\vr) = \PsiH$ (Gaussian Type Orbital)
            \item Ondes planes $\phi_j(\vr) = \exp\left(i\,\vec{K}.\vr\right)$ où $\vec{K}$ est un vecteur d'onde.
        \end{itemize}
    \end{itemize}
    \end{block}

    \vfill

    \begin{minipage}[c]{.6\textwidth}
         Dans une base d'ondes planes :
         \begin{equation*}
            \Psi_{\vk}(\vr) = \sum_{m=1}^{N_{maille}} \exp\left[i\,(\vk+\vec{K})\cdot\vr_m\right]
        \end{equation*}
        La taille de la base est fixée par la valeur maximale de l'énergie $\eps$ d'une onde plane (\textit{cut-off})
        \begin{equation*}
            \eps = \hbar^2|\vk+\vec{K}|^2 \, / \, 2m_e
        \end{equation*}
    \end{minipage}
    \hfill
    \begin{minipage}[c]{.34\textwidth}
	    \begin{exampleblock}{Avantages}
	        \begin{itemize}
	            \item Adaptées à la symétrie cristalline.
	            \item Un seul paramètre : l'énergie de \textit{cut-off}
	        \end{itemize}
	    \end{exampleblock}
    \end{minipage}
\end{frame}


% ------------------------------------------------------------------------------
\subsubsection[Pseudopotential]{Pseudopotential approach for core electrons}

\begin{frame}
    \frametitle{Space decomposition - Long range / short range}
    \framesubtitle{Pseudo-potential approach}

    \vspace*{-5mm}
    \begin{center}%
        \only<1>{\includegraphics[width=.8\textwidth]{orbitals_1}}%
        \only<2>{\includegraphics[width=.8\textwidth]{orbitals_2}}%
    \end{center}
    
    \pause[2]
    \vspace*{-5mm}
    \begin{itemize}
        \item Close to the nucleus there is strong oscillation of the wavefunction 
        \item Far away the nucleus the wavefunction decrease smoothly
    \end{itemize}

    \structure{Issues}
    \begin{itemize}
        \item How to describe simultaneously both regions ?
        \item Loss of wavefunction in core region.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Pseudopotentials}
    \framesubtitle{The Muffin-tin approach}
    
    \structure{Stating hypothesis}

    \begin{center}
        \large Mater is done from atoms and atoms are spherical.
    \end{center}

    \parbox{.6\textwidth}{\visible<2->{\includegraphics[width=.6\textwidth]{muffin-tin-pot}}}
    \parbox{.35\textwidth}{\visible<3->{
    \centering A Muffin tin
    
    \includegraphics[width=.35\textwidth]{muffin_tin}}
    }

    \pause[4]
    \structure{Representation}
        
    \begin{center}
    \begin{tikzpicture}[scale=.8, remember picture]
        \tikzstyle{atom} = [circle, draw, minimum width = 8mm, pattern={north east lines}]
        \draw (0,0) rectangle (3,3);
        \node[atom] at (3/4,3/4) {};
        \node[atom] at (3/4,9/4) {};
        \node[atom] at (9/4,3/4) {};
        \node[atom] at (9/4,9/4) {};
        \node[right, text width=8cm, xshift=5mm] at (current bounding box.east) {
            Space is thus divided in two regions:
            \begin{itemize}
                \item Volume in a sphere centered on an atom (\textit{"augmentation part"})
                \item Inter-sphere space
            \end{itemize}
        };
    \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Les pseudo-potentiels}
    \framesubtitle{\textit{Projector Augmented Wave} - PAW}

     \begin{center}
        \begin{minipage}{.51\textwidth}
        \begin{tikzpicture}
            \node at (0,0) {\includegraphics[scale=.25]{OA4s_paw}};
            \node at (.6,0.3) {$\psi$};
            \node at (-1.3,0.3) {$\tilde{\psi}$};
        \end{tikzpicture}
        \end{minipage}
        \hfill
        \begin{minipage}{.47\textwidth}
            \begin{itemize}
                \item The Schrödinger equation is solved with a pseudo-potential.
                
                \item In a sphere centered around each atom, the wavefunction is got \textit{a posteriori} 
            \end{itemize}
        \end{minipage}
    \end{center}

    \vspace{-5mm}        

    \begin{center}
    \begin{tikzpicture}[scale=.7, remember picture]
        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, pattern={north east lines}]
        \draw (0,0) rectangle (3,3);
        \node[atom] at (3/4,3/4) {};
        \node[atom] at (3/4,9/4) {};
        \node[atom] at (9/4,3/4) {};
        \node[atom] at (9/4,9/4) {};
        \node[above] at (1.5,3) {All electrons};
        
        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, dashed]
        \draw (4,0) rectangle (7,3);
        \node[atom] at (4+3/4,3/4) {};
        \node[atom] at (4+3/4,9/4) {};
        \node[atom] at (4+9/4,3/4) {};
        \node[atom] at (4+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (5.5,3) {pseudo function};

        \tikzstyle{atom} = [circle, draw, minimum width = 6mm]
        \draw[dashed] (8,0) rectangle (11,3);
        \node[atom] at (8+3/4,3/4) {};
        \node[atom] at (8+3/4,9/4) {};
        \node[atom] at (8+9/4,3/4) {};
        \node[atom] at (8+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (9.5,3) {pseudo function\\ in the spheres};
        
        \tikzstyle{atom} = [circle, draw, minimum width = 6mm, pattern={north east lines}]
        \draw[dashed] (12,0) rectangle (15,3);
        \node[atom] at (12+3/4,3/4) {};
        \node[atom] at (12+3/4,9/4) {};
        \node[atom] at (12+9/4,3/4) {};
        \node[atom] at (12+9/4,9/4) {};
        \node[above, text width = 3cm, align = center] at (13.5,3) {All electrons in the sphere};
                
        \node at (3.5,1.5) {\Large =};
        \node at (7.5,1.5) {\Large-};
        \node at (11.5,1.5) {\Large+};
        
        \node at (5.75,1.5) (s1) {};
        \node at (8.75,.75) (s2) {};
        \node at (12.75,.75) (s3) {};
    \end{tikzpicture}
    \end{center}

    \bigskip    
    
    \begin{equation*}
        \left\vert\psi\right\rangle
            = \tikz[remember picture]{\node (pseudo) {};}\hspace{-1.5ex}
            \left\vert\,\tilde{\psi}\, \right\rangle
            + \sum_i \left(\left\vert \phi_i \right\rangle 
              \tikz[remember picture]{\node (pseudo in sphere) {};}\hspace{-1.5ex}
            - \tikz[remember picture]{\node (ae in sphere) {};}\hspace{-1.5ex}
            \left\vert \tilde{\phi}_i \right\rangle
            \right) 
            \left\langle\tilde{\textrm{p}}_i\left\vert\tilde{\psi}\right\rangle\right.
    \end{equation*}
    
    \begin{tikzpicture}[overlay, thick, -stealth, remember picture, color=Ubleu]
        \draw ($(pseudo) + (.2,.25)$) to[bend left] (s1);
        \draw ($(pseudo in sphere) + (-.35,.25)$) to[bend left] (s2.center);
        \draw ($(ae in sphere) + (0.1,.25)$) to[out=50, in=230] (s3.center);
    \end{tikzpicture}
\end{frame}

\begin{frame}
    \frametitle{Plane waves basis set}
    \framesubtitle{An example with a Slater type orbitals}

    Consider the radial part of a Slater type orbital with $n$ = 3.
    \begin{equation*}
        \varphi_{n,0,0}^{slater}(r) = \frac{1}{\sqrt{(2n)!}} \left(\frac{2 Z}{n a_o}\right)^{n+\frac{1}{2}}
        r^{n-1} \exp\left(-\frac{Z r}{n a_o}\right)
    \end{equation*}
    
    \begin{center}%
        \only<1>{\includegraphics[width=.8\textwidth]{fit_slater3s_1}}%
        \only<2>{\includegraphics[width=.8\textwidth]{fit_slater3s_5}}%
        \only<3>{\includegraphics[width=.8\textwidth]{fit_slater3s_10}}%
        \only<4>{\includegraphics[width=.8\textwidth]{fit_slater3s_15}}%
        \only<5>{\includegraphics[width=.8\textwidth]{fit_slater3s_20}}%
    \end{center}
    
    \pause[4]
    \vspace*{-6mm}
    \begin{center}
    With only 15 plane waves we got a reasonable agreement against 15 plane waves with a True 3s atomic orbital.
    \end{center}
\end{frame}

\begin{frame}
    \frametitle{Convergence example}
    \framesubtitle{\ce{Na2S} - antifluorine compounds}

    \parbox[c]{5cm}{\structure{\ce{Na2S} - Na pseudopotential}\\Na : \ce{1s^2 2s^2 2p^6 3s^1}}
    \hfill
    \parbox[c]{4cm}{\includegraphics[width=4cm]{Li2O}}
    
    \begin{center}
        \includegraphics[width=.7\textwidth]{encut_Na2S}
    \end{center}
    
    \begin{itemize}
        \item Deeper electrons need plane waves at higher energy
    \end{itemize}
\end{frame}