
Résultats : Étude d'une surface
===============================

.. _resultats_surface:

Optimisation de la géométrie
----------------------------

.. figure:: img/opt_Cu111.png
   :width: 75%
   :align: center

   Position initiale en vert, position finale en bleu.


* Pas de modification au centre du modèle
* Tassement des couches de surface (relaxation perpendiculaire à la surface)
* Peu de modification dans le plan de la surface (relaxation parallèle à la surface)

Retour à :ref:`part_4_Cu111`.

Calcul de l'énergie de formation de surface
-------------------------------------------

Convergence de l'énergie de formation de surface en fonction du nombre de couches


.. figure:: img/gamma111.png
    :width: 100%
    :align: center


