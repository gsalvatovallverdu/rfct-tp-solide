Résultats : Structure électronique et magnétisme
================================================

.. _resultats_ni_magmom:

Ni métal - valeurs de MAGMOM
----------------------------

Avec MAGMOM = 1
~~~~~~~~~~~~~~~

.. code-block:: text

     total charge

    # of ion     s       p       d       tot
     ----------------------------------------
       1        0.496   0.488   8.334   9.319
        

     magnetization (x)

    # of ion     s       p       d       tot
     ----------------------------------------
       1       -0.007  -0.026   0.625   0.592
             
    E0= -.54582153E+01 eV


* On remarque que le nombre d'électrons est faux => projection onde planes / OA
  inexacte
* La magnétisation sur le nickel est non nulle => cohérant avec la présence
  d'électrons célibataires.

Avec MAGMOM = 0
~~~~~~~~~~~~~~~

.. code-block:: text

     magnetization (x)

    # of ion     s       p       d       tot
     ----------------------------------------
       1        0.000   0.000   0.000   0.000
         
    E0= -.54071349E+01 



* On remarque que le moment magnétique est nul contrairement au cas avec 
  ``MAGMOM = 1``
* L'énergie de la solution diamagnétique est plus élevée (:math:`\Delta E` = 51 meV)
* Pour un calcul spin polarisé (``ISPIN = 2``) il faut guider VASP pour trouver la
  solution de plus basse énergie.

Retour à :ref:`part_2_Ni`.

.. _resultats_ni_dos:

Ni métal -  densité d'états
---------------------------

DOS totale
~~~~~~~~~~

.. figure:: img/dos_Ni.jpg
  :width: 100%
  :align: center

  Densité d'états de Ni métal

* Absence de gap => composé métallique
* Dissymétrie de la DOS => :math:`n_\alpha - n_\beta \neq 0`.


Projection de la DOS sur les OA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sur la figure ci-dessous on retrouve la contribution de chaque OA du nickel à la DOS
totale.

.. figure:: img/dos_Ni_OA.jpg
  :width: 100%
  :align: center

  Contribution des OA à la densité d'états



Projection de la DOS sur les atomes avec VASP
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La projection de la fonction d'onde développée sur la base d'onde plane sur une base
d'orbitale atomique n'est pas exacte. De ce fait, la somme des contributions atomiques à
la DOS n'est parfois pas égale à la DOS totale notamment dans le cas des composé covalent
ou métallique.

Cas du Nickel : L'effet est faible mais les deux DOS devrait être superposées.

.. figure:: img/Ni_intersphere.jpg
  :width: 100%
  :align: center

  Comparaison entre la DOS totale et la somme des DOS projetées

Cas du silicium en faisant varier le rayon de la sphere, centrée sur chaque atome, dans 
laquelle on projette la DOS. Ici on voit clairement que suivant le rayon, la somme des DOS
projetées n'est pas égale à la DOS totale.

.. figure:: img/DOS_Si_rwings.jpg
  :width: 100%
  :align: center

  Effet du rayon de la sphere utilisé pour la projection des la densité électronique sur des OA


Conclusion : Avec VASP, il faut être vigilant lorsqu'on projette une DOS sur les atomes
(voir `bader <http://theory.cm.utexas.edu/bader/>`_).

Retour à :ref:`part_2_Ni`.

.. _resultats_ni_bandes:

Ni métal - diagramme de bandes
------------------------------

.. figure:: img/diagramme_Ni.jpg
  :width: 100%
  :align: center

  Diagramme de bandes et densité d'états de Ni métal



- Observer la correspondance entre le diagramme de bandes et la DOS

Retour à :ref:`part_2_Ni`.

Cu métal - diagramme de bandes
------------------------------

La figure suivante présente le diagramme de bandes et la densité d'états
du Cu métal. Les bandes d'énergies sont colorées en fonction de la contribution
des orbitales atomiques de type s (rouge), p (vert) ou d (bleu) dans
l'orbitale cristalline.

.. figure:: img/bands_Cu.png
  :width: 100%
  :align: center

  Diagramme de bandes et densité d'états de Cu métal.


