
Résultats : Base d'ondes planes et grille de points K
=====================================================

.. _resultats_cvg_encut:

Convergence de l'énergie en fonction du ENCUT 
---------------------------------------------

.. figure:: img/encut.jpg
   :width: 100%
   :align: center


* La convergence n’est pas variationnelle => mixing de la densité.
* La précision du calcul est de l’ordre de 10 meV.
* ENCUT > 400 eV

Retour à :ref:`part_1_Cu`.


.. _resultats_cvg_kpoints:

Grille de points k
------------------

Convergence des paramètres de maille et de l'énergie en fonction de la grille de points k

.. code-block:: bash

    #  k    a      alpha     Econv           SinglePts        DE
    #                        (eV)             (eV)          (meV)
       1   2.238    60.0  -15.8630060      -14.1349960   -14134.996
       2   2.701    60.0   -2.3472513       -2.2458515    11889.144
       3   2.497    60.0   -4.6616245       -4.6293625    -2383.511
       4   2.529    60.0   -4.1397856       -4.1322644      497.098
       5   2.515    60.0   -4.4135761       -4.3978349     -265.570
       6   2.525    60.0   -4.2458045       -4.2366917      161.143
       7   2.518    60.0   -4.3532290       -4.3395484     -102.857
       8   2.524    60.0   -4.2771373       -4.2673145       72.234
       9   2.519    60.0   -4.3250466       -4.3123058      -44.991
      10   2.519    60.0   -4.2970530       -4.2843054       28.000
      11   2.518    60.0   -4.3181305       -4.3041724      -19.867
      12   2.518    60.0   -4.3085235       -4.2950313        9.141



``Econv`` est l'énergie de la structure optimisée, ``SinglePts`` est l'énergie obtenue à
la fin du premier cycle SCF c'est à dire l'énergie de la structure de départ. L'intérêt de
la colonne ``SinglePts`` est de pouvoir comparer l'effet de la grille de
points k sur l'énergie pour une même structure.

* Les paramètres de maille convergent rapidement.
* L'énergie nécessite une grille plus importante => Système métallique

Retour à :ref:`part_1_Cu`.
