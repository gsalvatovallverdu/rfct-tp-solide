Résultats : Correction de Hubbard
=================================

.. _correction_NiO_mag:

Moment magnétique de NiO
------------------------

Sans correction de Hubbard, l'optimisation de la densité électronique de NiO conduit aux
résulats suivants :

.. code-block:: text

          N       E                     dE             d eps       ncg     rms          rms(c)
    DAV:   1     0.170069147592E+03    0.17007E+03   -0.11852E+04  1080   0.143E+03
    DAV:   2    -0.206284106087E+02   -0.19070E+03   -0.18041E+03  1060   0.335E+02
    DAV:   3    -0.311792171690E+02   -0.10551E+02   -0.10104E+02  1168   0.943E+01
    DAV:   4    -0.313299869103E+02   -0.15077E+00   -0.14970E+00  1124   0.119E+01
    DAV:   5    -0.313322749937E+02   -0.22881E-02   -0.22782E-02  1148   0.131E+00    0.152E+01
    RMM:   6    -0.268644590599E+02    0.44678E+01   -0.14496E+01  1076   0.367E+01    0.434E+00
    RMM:   7    -0.268573750866E+02    0.70840E-02   -0.10812E+00  1116   0.632E+00    0.312E+00
    RMM:   8    -0.268136432490E+02    0.43732E-01   -0.31912E-01  1074   0.601E+00    0.212E+00
    RMM:   9    -0.267887385214E+02    0.24905E-01   -0.15059E-01  1092   0.365E+00    0.106E+00
    RMM:  10    -0.267769266674E+02    0.11812E-01   -0.27003E-02  1096   0.222E+00    0.152E-01
    RMM:  11    -0.267771627609E+02   -0.23609E-03   -0.13419E-03  1115   0.442E-01    0.240E-01
    RMM:  12    -0.267765963561E+02    0.56640E-03   -0.17021E-03  1089   0.358E-01    0.378E-02
    RMM:  13    -0.267766040669E+02   -0.77108E-05   -0.46640E-05  1110   0.916E-02    0.254E-02
    RMM:  14    -0.267765954812E+02    0.85857E-05   -0.68167E-05  1110   0.743E-02    0.144E-02
    RMM:  15    -0.267765943951E+02    0.10861E-05   -0.33016E-06   807   0.214E-02    0.212E-03
    RMM:  16    -0.267765943590E+02    0.36181E-07   -0.52743E-07   590   0.625E-03
       1 F= -.26776594E+02 E0= -.26776594E+02  d E =0.000000E+00  mag=     0.0000

On remarque que le moment magnétique total est nul. Ceci est cohérent avec l'ordre
magnétique antiferromagnétique de NiO, suggéré par la valeur de ``MAGMOM`` dans le fichier
``INCAR``.

Les valeurs de ``MAGMOM`` ne sont pas utilisées comme des contraintes. Il 
s'agit de la configuration initiale du calcul. Durant l'optimisation il est 
possible que VASP change les signes des moments magnétiques ou que les valeurs
s'éloignent fortement des valeurs de ``MAGMOM`` initiale. Pour fixer la 
magnétisation totale du système, on peut utiliser ``NUPDOWN``.

Dans le fichier ``OUTCAR`` on relève les valeurs de moments magnétiques :

.. code-block:: text

    magnetization (x)
     
    # of ion     s       p       d       tot
    ----------------------------------------
        1       -0.012  -0.014   1.240   1.214
        2        0.012   0.014  -1.240  -1.214
        3        0.000   0.000   0.000   0.000
        4        0.000   0.000   0.000   0.000
    ------------------------------------------------
      tot        0.000   0.000   0.000   0.000

On voit que le moment magnétique sur chaque atome de nickel est de signe opposé ce qui
correspond à l'ordre antiferromagnétique.

Retour à :ref:`part_3_NiO`.

Analyse de la DOS de NiO
------------------------

.. _correction_NiO_dos_DFT:

Calcul DFT
~~~~~~~~~~

.. figure:: img/NiO_DOS.jpg
   :align: center
   :width: 100%

   Densité d'état de NiO sans correction de Hubbard.

On remarque :

* Absence de gap, ou gap très faible (0.22 eV) alors que NiO est un isolant.
* Les OA 2s de l'atome d'oxygène sont profondes et peu dispersées (solide ionique).
* La DOS est parfaitement symétrique, donc :math:`n_\alpha = n_\beta`, ce qui
  correspond à une situation antiferromagnétique.
   
.. _correction_NiO_dos_DFT+U:

Calcul DFT+U, U = 8 eV
~~~~~~~~~~~~~~~~~~~~~~

.. figure:: img/NiO_U8_DOS.jpg
   :align: center
   :width: 100%

   Densité d'état de NiO avec une correction de Hubbard.


On remarque :

* ouverture du gap :math:`\simeq` 3.4 eV
* Les OA d se sont éloignées du niveau de fermi => effet du U

.. figure:: img/NiO_U8_t2g_eg.jpg
   :align: center
   :width: 100%

   Densité d'état de NiO avec la contribution des niveau eg et t2g.

Du fait de l'environnement octaédrique des atomes de Nickel dans NiO, on retrouve la
séparation des niveaux t2g et eg. Le nickel dans sa forme Ni(II) étant :math:`d^8`, les niveaux eg ne sont pas
complètement remplis ce qui se voit sur la DOS puisque les niveaux t2g n'appraissent plus
dans la bande de conduction.

Densité d'états en fonction de U
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: img/NiO_U.jpg
   :align: center
   :width: 100%

   Effet de la valeur du paramètre U sur la densité de d'état de NiO.


Observations :

* Le gap s'ouvre progressivement
* Les OA d du nickel sont progressivement stabilisées sous l'effet du U

.. _correction_NiO_moment:

Moment magnétique en fonction de U
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: img/NiO_moment.jpg
    :align: center
    :width: 100%

    Valeur du moment magnétique du Ni dans NiO en fonction de la valeur du paramètre U.

Observations :

* En fonction de U on augmente progressivement la valeur du moment magnétique.
* Pour une valeur de U comprise entre 7 et 8 eV on retrouve la valeur du moment
  magnétique expérimentale.

Retour à : :ref:`part_3_NiO`.
