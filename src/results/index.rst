=========================
Résultats et commentaires
=========================

Dans chaque partie des liens intitulés "Résultats" vous renvoient vers les pages
ci-dessous contenant les résultats des calculs demandés ainsi que quelques commentaires.

.. toctree::
   :maxdepth: 2

   correction_Cu
   correction_Ni
   correction_NiO
   correction_Cu111