=================
Travaux pratiques
=================

Le TP/tutorial se décompose en 4 parties et a pour objectif de survoler rapidement les
étapes clefs pour choisir les conditions de calculs ainsi que pour calculer des propriétés
électroniques telles que la densité d'états, le diagramme de bandes ou le magnétisme :

.. toctree::
   :maxdepth: 2

   bilbo
   environnement
   first
   part_1_Cu
   part_2_Ni
   part_3_NiO
   part_4_Cu111

