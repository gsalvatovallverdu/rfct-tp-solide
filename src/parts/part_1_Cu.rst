
.. angstrom declaration
.. |AA| unicode:: U+212B

.. _part_1_Cu:

Base d'onde plane et grille de points K
=======================================

Objectif :

* voir comment tronquer la base d'onde plane et choisir la grille de points
  k. L'idée est d'obtenir un compromis entre temps de calcul et précision.


Le système étudié est le cuivre métal (CFC, :math:`Fm\overline{3}m`,
a = 3.620 |AA|). Vous utiliserez la maille unitaire rhomboédrique, 
en rouge sur la :ref:`figure 1 <struct_Cu>`, qui contient un atome de cuivre 
par maille.

.. _struct_Cu: 

.. figure:: img/cu.png
   :width: 75%
   :align: center

   Maille cubique face centrée du cuivre (en noir) et la maille 
   unitaire rhomboédrique (en rouge).

Dans le code VASP, la base d'onde plane est déterminées par une valeur de 
*cut-off* correspondant à l'énergie cinétique maximale des ondes planes 
constituant la base. Cette valeur est donnée par le mot clef ``ENCUT`` dans 
le fichier ``INCAR``. La grille de points k, est définie dans le fichier 
``KPOINTS``.

Effet du ENCUT
--------------

**1)** Pour commencer on travaille sur la structure expérimentale et on fait un 
simple calcul d'énergie (*single point*). Utiliser un seul point k (le point 
:math:`\Gamma`), soit une grille 1x1x1 dans le fichier ``KPOINTS`` et faire 
varier la valeur de ``ENCUT`` de 200 à 800 eV dans le  fichier ``INCAR``.

:download:`Fichiers nécessaires pour faire cette partie <inputs/Cu_encut.zip>`.

**2)** Tracer l'énergie en fonction de la valeur de ``ENCUT`` et analyser la 
convergence du calcul. Est elle variationnelle ? Pour quelle valeur de 
``ENCUT`` la base semble-t-elle convergée ?

:ref:`voir les résultats <resultats_cvg_encut>`.

Effet de la grille de points K
------------------------------

On va maintenant voir l'effet de la grille de points k définie dans l'espace
réciproque sur la qualité des calculs. On comparera à la fois l'aspect 
structural et l'aspect énergétique.

Utiliser la valeur de ``ENCUT`` déterminée précédemment et optimiser la géométrie pour 
différentes grilles de points k (fichier ``KPOINTS``). Faire varier la 
densité de points dans l'espace réciproque en prenant des grilles de 
1x1x1 à 12x12x12.

:download:`Fichiers nécessaires pour faire cette partie <inputs/Cu_kpoints.zip>`.

Vous pouvez utiliser :ref:`le script proc.sh <proc_kpoints>` fourni avec les 
fichiers inputs pour générer automatiquement les fichiers ``KPOINTS`` et lancer
les calculs. Pour utilsier le script, il faut se trouver dans une session 
interactive de slurm (après avoir fait ``salloc``).

Comparer les paramètres de maille optimisés aux valeurs expérimentales et 
conclure. Les paramètres de maille optimisés peuvent être obtenus à partir 
du fichier ``CONTCAR``. La commande suivante calcule les paramètres
à partir des vecteurs de maille du fichier CONTCAR (ou POSCAR) :

.. code-block:: console

   (pmg) username@slurm-ens-frontal:~/TP_VASP/Cu_kpoints$ v cell CONTCAR
   --------------------------------------------------
   POSCAR : Cu metal rhomboedrique
   --------------------------------------------------
   a     =    2.55973
   b     =    2.55973
   c     =    2.55973
   alpha =     60.000
   beta  =     60.000
   gamma =     60.000
   --------------------------------------------------


Vous pouvez utiliser :ref:`le script ana_kpoints.py <ana_Cu_kpoints>` fourni avec les
fichiers inputs pour extraire les données intéressantes (paramètres de maille,
énergies ...).

:ref:`Résultats <resultats_cvg_kpoints>`.

