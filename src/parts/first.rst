==============
Premier calcul
==============

Pour commencer, on effectue un calcul sur un système simple : Li métal.
Voici la liste de commandes conduisant à l'exécution de VASP. Dans l'ordre

1. on charge le module associé au TP
2. on crée un dossier pour faire le calcul et on se déplace dans ce dossier
3. on copie le fichier POTCAR du lithium dans le dossier
4. on exécute VASP (si vous avez fait ``salloc`` utilisez ``mpiexec -np 1 vasp``)

.. code-block:: console

    (pmg) username@slurm-ens1:~/TP_VASP$ module load TP_VASP/2022
    (pmg) username@slurm-ens1:~/TP_VASP$ mkdir Li
    (pmg) username@slurm-ens1:~/TP_VASP$ cd Li/
    (pmg) username@slurm-ens1:~/TP_VASP/Li$ ls
    INCAR  KPOINTS  POSCAR
    (pmg) username@slurm-ens1:~/TP_VASP/Li$ cp $PAWROOT/Li/POTCAR .
    (pmg) username@slurm-ens1:~/TP_VASP/Li$ ls
    INCAR  KPOINTS  POSCAR  POTCAR
    (pmg) username@slurm-ens1:~/TP_VASP/Li$ vasp
    running on    1 total cores
    distrk:  each k-point on    1 cores,    1 groups
    distr:  one band on    1 cores,    1 groups
    using from now: INCAR
    vasp.5.3.5 31Mar14 (build Oct 16 2018 09:07:22) complex
    POSCAR found type information on POSCAR  Li
    POSCAR found :  1 types and       2 ions
    scaLAPACK will be used

    -----------------------------------------------------------------------------
    |                                                                             |
    |  ADVICE TO THIS USER RUNNING 'VASP/VAMP'   (HEAR YOUR MASTER'S VOICE ...):  |
    |                                                                             |
    |      You enforced a specific xc-type in the INCAR file,                     |
    |      a different type was found on the POTCAR file                          |
    |          I HOPE YOU KNOW, WHAT YOU ARE  DOING                               |
    |                                                                             |
    -----------------------------------------------------------------------------

    LDA part: xc-table for Pade appr. of Perdew
    POSCAR, INCAR and KPOINTS ok, starting setup
    FFT: planning ...
    WAVECAR not read
    entering main loop
        N       E                     dE             d eps       ncg     rms          rms(c)
    DAV:   1    -0.326512182789E+01   -0.32651E+01   -0.38981E+02   225   0.167E+02
    DAV:   2    -0.378829713122E+01   -0.52318E+00   -0.49814E+00   255   0.115E+01
    DAV:   3    -0.379599912787E+01   -0.77020E-02   -0.76306E-02   275   0.147E+00
    DAV:   4    -0.379600691783E+01   -0.77900E-05   -0.77900E-05   255   0.455E-02
    DAV:   5    -0.379600697487E+01   -0.57035E-07   -0.57031E-07   265   0.288E-03    0.192E-01
    DAV:   6    -0.379457814890E+01    0.14288E-02   -0.41812E-05   215   0.419E-02    0.126E-01
    DAV:   7    -0.379369076385E+01    0.88739E-03   -0.11252E-04   215   0.733E-02    0.559E-03
    DAV:   8    -0.379367069165E+01    0.20072E-04   -0.13237E-06   230   0.657E-03    0.221E-03
    DAV:   9    -0.379366697861E+01    0.37130E-05   -0.38801E-07   120   0.402E-03    0.927E-05
    DAV:  10    -0.379366700347E+01   -0.24854E-07   -0.13700E-08   115   0.636E-04
    1 F= -.37936670E+01 E0= -.37917964E+01  d E =-.561187E-02
    writing wavefunctions


Le calcul dure quelques secondes. On peut voir sur la sortie standard des
informations concernant la lecture des fichiers inputs puis les itérations
conduisant à la relaxation de la densité électronique (processus SCF).
``DAV`` indique l'utilisation d'un algorithme *block-davidson*. Lors que
``dE`` et ``d eps`` sont inférieurs à la valeur ``EDIFF`` précisée dans le fichier
INCAR, le calcul s'arrête.

:download:`Fichiers nécessaires pour faire cette partie <inputs/Li.zip>`.
