
.. angstrom declaration
.. |AA| unicode:: U+212B

.. _part_2_Ni:

Structure électronique et magnétisme
====================================

Objectifs :

* Tracer une densité d'état (DOS).
* Tracer un diagramme de bandes.
* Étudier un composé couche ouverte.

:download:`Fichiers nécessaires pour faire cette partie <inputs/Ni.zip>`.

Le système étudié est le nickel métal (CFC, :math:`Fm\overline{3}m`,
a = 3.52387 |AA|). Comme pour le cuivre on utilisera la maille 
unitaire rhomboédrique. On ne cherchera pas à optimiser la structure 
pour se concentrer sur les propriétés électroniques.

Calcul single point
-------------------

Un calcul "single point" est un calcul dans lequel on n'optimise pas la
structure du système (NSW = 0). Seule la densité électronique est relaxée 
via un cycle SCF.

Relaxer la densité électronique de Ni métal, relever les 
valeurs de moment magnétique et le nombre d'électrons portés 
par le nickel à la fin du fichier ``OUTCAR``. Que constatez vous ?

Effet du MAGMOM
---------------

Dans le fichier ``INCAR``, le mot clef ``MAGMOM`` indique le nombre 
d'électrons célibataires sur chaque atome pour initialiser la densité 
électronique. Attribuer les valeurs 0. puis 2. à ``MAGMOM``, relever 
les valeurs du moment magnétique et conclure.

:ref:`Résultats <resultats_ni_magmom>`.

Calcul de la densité d'états
----------------------------

L'objectif est de calculer la densité d'état pour chaque état magnétique 
obtenu précédemment. 

Pour calculer une DOS, il est nécessaire d'avoir au préalable une 
densité électronique convergée (un fichier CHGCAR). 

**a)** Si nécessaire, relancer un calcul avec la valeur choisie du ``MAGMOM``.

**b)** Créer un sous dossier ``DOS`` et copier les fichiers ``INCAR``, 
``POSCAR``, ``KPOINTS``, ``POTCAR`` et ``CHGCAR`` à l'intérieur :

.. code-block:: console

    (pmg) username@@slurm-ens-frontal:~$ mkdir DOS
    (pmg) username@@slurm-ens-frontal:~$ cp INCAR POSCAR POTCAR KPOINTS CHGCAR DOS/
    (pmg) username@@slurm-ens-frontal:~$ cd DOS/


**c)** Dans le fichier ``INCAR`` ajouter le mot clef ``ICHARG`` avec la valeur 11 
et dans le fichier ``KPOINTS`` demander une grille de points k 21x21x21.
Un calcul de DOS nécessite une grande densité de points k.

``ICHARG = 11`` signifie que le calcul ne suit pas un processus SCF et ne modifiera 
pas la densité électronique. On utilise la densité électronique fournie via le
fichier ``CHGCAR``.
On utilise cette technique pour augmenter le nombre 
de points k dans le but d'obtenir une DOS plus précise en limitant le temps de calcul.

**d)** Utiliser la commande ``v tdos`` pour visualiser la DOS totale et observer 
la dissymétrie entre la DOS des électrons :math:`\alpha` et :math:`\beta` caractéristique
d'un système ferromagnétique.

.. code-block:: console
    
    (pmg) username@@slurm-ens-frontal:~$ v tdos

**e)** Pour pour visualiser la contribution des OA de type s, p et d à la DOS 
totale, utilisez la commande ``v pdos``.

.. code-block:: console
    
    (pmg) username@@slurm-ens-frontal:~$ v pdos


Reprendre les étapes a) à e) en changeant la valeur de MAGMOM et voir l'effet 
sur la densité d'états.


:ref:`Voir les résultats de cette partie <resultats_ni_dos>`.


Calcul du diagramme de bandes
-----------------------------

**a)** Comme pour le calcul de la DOS, copier dans un nouveau dossier les fichiers 
``INCAR``, ``POSCAR``, ``KPOINTS``, ``POTCAR`` et ``CHGCAR``, par exemple :

.. code-block:: console

    (pmg) username@@slurm-ens-frontal:~$ mkdir Bandes
    (pmg) username@@slurm-ens-frontal:~$ cp INCAR POSCAR POTCAR KPOINTS CHGCAR Bandes/
    (pmg) username@@slurm-ens-frontal:~$ cd Bandes/


**b)** Dans le fichier ``KPOINTS``, il faut définir les lignes de l'espace réciproque 
le long desquelles seront calculées les bandes d'énergies. Voici le fichier ``KPOINTS`` 
pour un diagramme de bandes qui passe par les points 
L -> :math:`\Gamma` -> X -> U puis K -> :math:`\Gamma` de l'espace réciproque :

.. code-block:: bash
    :caption: fichier KPOINTS_bands

    k-points for bandstructure L-G-X-U K-G
    100
    line
    reciprocal
    0.50000  0.50000  0.50000    1
    0.00000  0.00000  0.00000    1

    0.00000  0.00000  0.00000    1
    0.00000  0.50000  0.50000    1

    0.00000  0.50000  0.50000    1
    0.25000  0.62500  0.62500    1

    0.37500  0.7500   0.37500    1
    0.00000  0.00000  0.00000    1


Coordonnées des points dans l'espace réciproque :

===============      ===== ===== =====
points               kx    ky    kz   
===============      ===== ===== =====
:math:`\Gamma`       0.0   0.0   0.0  
L                    0.5   0.5   0.5  
X                    0.0   0.5   0.5  
U                    0.25  0.625 0.625
K                    0.375 0.75  0.375
===============      ===== ===== =====

Vous pouvez utilisez le fichier ``KPOINTS_bands``.

**c)** Dans le fichier ``INCAR``, comme pour le calcul de DOS, ajouter ``ICHARG = 11``.
Il faut également changer les paramètres du smearing. Modifier le fichier INCAR
en incluant ou modifiant les valeurs suivantes :

.. code-block:: bash

    ICHARG = 11
    ISMEAR = 1
    SGIMA = 0.2


Dans le cas du diagramme de bandes, pourquoi est il obligatoire d'utiliser 
une densité électronique fixe et de ne pas la recalculer en utilisant la
grille de points k fournie ?

**d)** Lancer le calcul du diagramme de bandes. Pour 
visualiser le diagramme de bandes utiliser la commande ``v bands``. 
Le comparer à la DOS précédemment obtenue.

.. code-block:: console
    
    (pmg) username@@slurm-ens-frontal:~$ v bands


Comme pour la DOS, vous pouvez reprendre les étapes a à d avec une valeur
de MAGMOM différente.

:ref:`Voir les résultats de cette partie <resultats_ni_bandes>`.

