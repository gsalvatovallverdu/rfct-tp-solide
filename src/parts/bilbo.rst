====================
Utilisation de Bilbo
====================

Pour l'enseignement, l'UPPA dispose 
d'une machine de calcul de taille réduite (mais conséquente) qui s'appelle ``bilbo``
(rapport à la taille réduite).
Lors de ce TP les calculs seront réalisés sur la machine de calcul ``bilbo``.
Cette page vous présente comment se connecter et utiliser le serveur
de calcul pour mettre en œuvre des calculs avec le logiciel VASP.

L'accès au serveur de calcul ``bilbo`` se fait via les salles de TP 
d'informatique de l'UPPA. Depuis l'extérieur l'accès est autorisé via
un VPN.

Configuration du VPN
====================

La première étape consiste à télécharger votre profil VPN à l'UPPA et 
d'installer un logiciel permettant d'activer le VPN.
Le VPN permet de naviguer sur internet en étant authentifié à l'UPPA. Il vous 
permet donc d'accéder, depuis chez vous, aux ressources qui sont en temps normal 
uniquement disponibles depuis l'UPPA.

Tout les étapes sont détaillées sur la page : https://vpn.univ-pau.fr/

Faites bien attention à suivre les étapes concernant les étudiants ou les 
personnels suivant votre cas.

Avant d'installer et activer votre VPN, pensez à vous rendre sur votre 
application `moncompte <https://moncompte.univ-pau.fr/>`_ et activer l'accès
à internet. Une fois connecté sur l'application :

* rendez-vous sur le menu "Mon compte" -> "Mon accès VPN". 
* Descender sur le cadre "Accès internet depuis le VPN" 
* cocher la case "Autoriser l'accès internet depuis le VPN"

Connection ssh via un terminal
==============================

La machine bilbo est accessible via un terminal par ssh. Une fois le
VPN activé, pour vous
connecter, entrer la commande suivante en remplaçant ``username`` par
votre login UPPA (facultatif si votre login est identique sur la machine 
depuis laquelle vous vous connectez) :

.. code-block:: console

    $ >  ssh -Y username@bilbo.univ-pau.fr
    Linux slurm-ens-frontal 4.19.0-23-amd64 #1 SMP Debian 4.19.269-1 (2022-12-20) x86_64
    ————————————————————————————————————————————————————
                /!\ Managed by Ansible /!\
        Local modifications may be overwritten
    Contact dn-infra-sys@univ-pau.fr for more info
    ————————————————————————————————————————————————————
        List of services managed by ansible:
            - ssh
            - ntp
            - unattended-upgrades
            - firehol
            - apt
            - collectd
            - exim4
            - slurm-core
            - slurm-frontal
    ————————————————————————————————————————————————————

    Last login: Wed Jan 18 15:12:15 2023 from 10.10.13.10

    username@slurm-ens-frontal:~$

N'oubliez pas l'option ``-Y`` pour être en capacité d'exéctuer des programme
graphique sur le serveur.

Connection graphique avec x2go
==============================

X2go est un logiciel de bureau distant qui vous permettra de vous connecter 
sur le serveur bilbo (dans le cadre du TP) sur lequel se feront les calculs.

Configuration de x2go
---------------------

x2go existe en deux versions : une version serveur et une version client. 
Dans votre cas, vous avez besoin de la version client. Vous êtes un client 
au sens que vous souhaitez accéder à un serveur.

Rendez vous `sur le site de x2go <https://wiki.x2go.org/doku.php/download:start>`_ 
et télécharger la version qui vous concerne (linux, macos, windows ...). Installer ensuite x2go.

Une fois x2go installé, rendez vous dans : Session -> Nouvelle session.

Une fenêtre apparaît que vous devez compléter comme indiqué ci-dessous. 
Vous n'avez besoin que de configurer la partie session (premier onglet).

* Nom de la session : Un nom qui vous covient, par exemple bilbo pour se rappeler 
  que cette session sert à se connecter à bilbo.
* Hôte : bilbo.univ-pau.fr
* Identifiant : votre login UPPA
* Type de session (en bas) : XFCE

Cliquer ensuite sur ok.

.. figure:: img/session.png
   :width: 100%
   :align: center


Connexion via x2go
------------------

Pour accéder à bilbo vous allez procéder en 2 étapes :

1. Activation du VPN : Ouvrir open VPN et activer le VPN
2. Connexion avec x2go

Pour la deuxième étape, ouvrir x2go et cliquer sur la session que 
vous avez créée précédemment.
Entrez ensuite votre mot de passe UPPA et cliquer sur ok. Lors de la première 
connexion, il vous sera demandé d'accepter l'empreinte (fingerprint) de bilbo. 
Cliquez sur yes.

Une nouvelle fenêtre doit alors apparaître avec un bureau similaire à une 
interface Linux (XFCE). Lors de la première connexion il vous sera demandé 
de configurer l'interface. Choisissez la configuration par défaut.

Vous êtes maintenant connecté sur biblo. À la fin du TP, fermer cette fenêtre 
et fermer votre session en cliquant sur le bouton éteindre dans la fenêtre 
principale de x2go.

