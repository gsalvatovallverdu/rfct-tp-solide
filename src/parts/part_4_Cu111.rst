.. _part_4_Cu111:

===================
Étude d'une surface
===================

Objectif :

* Modéliser un système 2D-périodique.

:download:`Fichiers nécessaires pour faire cette partie <inputs/Cu_surface.zip>`.

Dans cette partie vous travaillerez sur la surface (111) du cuivre, voir les figures ci-dessous.

.. figure:: img/Cu_111.png
   :width: 75%
   :align: center

   Plan de coupe du cuivre métal pour obtenir une surface (111)

.. figure:: img/Cu_111_model.png
   :width: 75%
   :align: center

   Modèle de surface (111) à 4 couches.

..

Calcul en volume
================

Dans un premier temps on calcule une valeur précise de l'énergie du Cu
métal en volume (ou énergie bulk). Pour accélérer les calculs dans le cadre
du TP on va utiliser des conditions assez basse. Il est important de garder
les même conditions pour tous les calculs, en particulier une grille de points
k cohérente entre les calculs 2D et 3D et une valeur de ENCUT identique.

Reprendre la maille rhomboédrique du cuivre métal vu en début de TP et optimiser 
la densité électronique (``NSW = 0``) avec une grille de points k 11x11x11 
(fichier ``KPOINTS``) et ajouter dans le fichier ``INCAR`` : 

.. code-block:: text

    ENCUT = 300
    EDIFF = 1.e-5
    ALGO = Fast

Avant de faire des calculs sur un modèle de surface, il est nécessaire de bien maîtriser 
les conditions de calcul sur la phase bulk. De plus, l'énergie du bulk intervient dans
le calcul de l'énergie de formation de surface :

.. math::

    \gamma_{hkl} = \frac{1}{2A} \left[ E_{surface, n}  - n E_{bulk} \right]

où :math:`E_{surface, n}` est l'énergie du modèle de surface, n le nombre de groupement 
formulaire dans le modèle de surface et :math:`E_{bulk}` l'énergie du bulk par groupement 
formulaire. Une autre approche consiste à calculer :math:`E_{bulk}` à partir de la différence
des énergies de deux modèles de surfaces avec un nombre de couches différent
(`Boettger, J. C. Phys. Rev. B 1994, 49, 16798–16800 <https://journals.aps.org/prb/abstract/10.1103/PhysRevB.49.16798>`_).

.. math::

    \gamma_{hkl} = \frac{1}{2A} \left[ E_{surface, n} - \frac{n}{n - m} (E_{surface, n} - E_{surface, m}) \right]

Dans la formule ci-dessus, l'énergie du "bulk" est obtenue à partir de la
différence d'énergie entre deux surfaces ayant un nombre de couches atomiques 
différent.

Effet du nombre de couches atomiques
====================================

L'étape suivante est de savoir combien de couches atomiques sont nécessaires 
pour représenter la surface. Pour ce faire, on regarde la convergence 
de :math:`\gamma_{hkl}` en fonction du nombre de couches. Ici il y a un 
atome de cuivre par couche, donc dans l'équation ci-dessus, n 
est directement le nombre de couches.

**a)** Préparer un fichier ``KPOINTS`` avec une grille de points k 11x11x1. 
Justifier la grille utilisée.

**b)** Optimiser la structure de la surface pour des modèles avec 1, 2, 3, 
et 4 couches et calculer :math:`\gamma_{111}`. Pour ce faire, dans le 
fichier ``INCAR``, ajouter les lignes suivantes relative à la relaxation 
de la structure 

.. code-block:: text

    Ionic relaxation
      NSW    = 40
      ISIF   = 2
      IBRION = 2
      EDIFFG = -0.02

Par ailleurs ajouter ``NELMIN = 4`` avec les mots clefs correspondant à 
la minimisation de la densité électronique. Cette option, force VASP à 
faire au minimum ``NELMIN`` itération SCF pour obtenir la nouvelle densité 
électronique et améliore la convergence.

**c)** Observer la convergence de :math:`\gamma_{111}` en fonction de n.

:ref:`Voir les résultats de cette partie <resultats_surface>`.

Vous pouvez calculer la densité d'état d'un des modèles et conclure sur 
l'effet de la formation de la surface sur les propriétés électronique 
du cuivre.
