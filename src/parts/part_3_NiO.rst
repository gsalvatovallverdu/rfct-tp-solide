
.. angstrom declaration
.. |AA| unicode:: U+212B

.. _part_3_NiO:

Correction de Hubbard
=====================

Objectifs :

* Mettre en oeuvre la correction de Hubbard pour des électrons d.
* Voir l'effet de cette correction sur les propriétés électroniques.
* Différence d'énergie entre deux ordres magnétiques

:download:`Fichiers nécessaires pour faire cette partie <inputs/NiO.zip>`.

Le système étudié est l'oxyde de nickel NiO (structure type NaCl, 
:math:`Fm\overline{3}m`, a = 4.17 |AA|). L'ordre magnétique le plus stable de 
NiO est un ordre antiferromagnétique (AF) le long de la direction [111]. Pour 
représenter correctement cet ordre magnétique on se place dans la maille magnétique 
représentée en vert sur la figure :ref:`figure 3 <struct_NiO>`. Comme précédemment, on ne 
cherchera pas à optimiser la structure pour se concentrer sur les propriétés 
électroniques.

.. _struct_NiO:

.. figure:: img/NiO.png
   :align: center
   :width: 75%

   Structure de NiO (type NaCl). Les atomes de nickel sont en gris et les atomes 
   d'oxygène en rouge. La maille noire est la maille cubique représentative. La 
   maille verte est la maille magnétique (rhomboédrique).

:download:`Fichier pour visualiser la maille avec VESTA <NiO.vesta>`
   

Calculs sans correction
-----------------------

**a)** Avec le mot clef ``MAGMOM``, initialiser les moments magnétiques à +2 et -2 pour 
les atomes de nickel et 0 pour les atomes d'oxygène. Relever l'énergie, et la 
valeur des moments magnétiques de chaque atome.

:ref:`Voir les résultats <correction_NiO_mag>`.

**b)** Analyse de la DOS de NiO. 

Calculer la DOS de NiO (grille de points k 10x10x10). 

Visualiser la DOS totale et la densité d'états projetée sur les OA des 
atomes de nickel et d'oxygène.

.. code-block:: console

    (pmg) username@slurm-ens-frontal:~$ v tdos
    gap =     0.220400
    (pmg) username@slurm-ens-frontal:~$ v pdos
    gap =     0.220400

Relever la valeur du gap, c'est à dire la largeur de l'intervalle pour lequel
la densité d'états au dessus du niveau de fermi est nulle.

Observer la contribution de chaque atome et identifier la partie de la
densité d'états qui correspond aux orbitales du bloc d. Quel atome et quelles
orbitales contribuent le plus à la densité d'états autour du niveau de fermi ?

:ref:`Voir les résultats de cette partie <correction_NiO_dos_DFT>`.

Calculs avec correction de Hubbard
----------------------------------

Modèle de Hubbard 
(`Dudarev, S. Phys. Rev. B 1998, 57, 1505-1509. 
<https://journals.aps.org/prb/abstract/10.1103/PhysRevB.57.1505>`_)

Reprendre les calculs précédents en ajoutant les lignes suivantes au fichier 
``INCAR``, sans les commentaires.

.. code-block:: text

    Hubbard model
        LDAU      = True         ! active LDA+U
        LDAUTYPE  = 2            ! approximation de Dudarev
        LDAUL     = 2 -1         ! e- d du Ni uniquement
        LDAUU     = 8.00  0.00   ! U = 8 eV
        LDAUJ     = 0.95  0.00   ! J = 0.95 eV
        LMAXMIX   = 4            ! convergence 

Reprendre l'analyse de la DOS et conclure sur l'effet de la correction.

:ref:`Voir les résultats de cette partie <correction_NiO_dos_DFT+U>`.

Comparer les valeurs des moments magnétiques et du gap aux valeurs expérimentales : 
gap :math:`\simeq` 4.0 eV, moment magnétique 1.70 :math:`\mu_B`.

:ref:`Voir les résultats de cette partie <correction_NiO_moment>`.

Configuration magnétique
------------------------

Jusqu'à présent, les moments magnétiques sur les atomes de nickel étaient 
de signes opposés. C'est ce qu'on appelle un ordre antiferromagnétique. 
Changer les valeurs du ``MAGMOM`` pour calculer l'énergie de l'ordre 
ferromagnétique dans lequel les moments magnétiques sont parallèles.
Conclure sur la configuration magnétique à l'état fondamental.

