========================
Environnement de travail
========================

Cette page vous indique comment configurer votre environnement de travail
pour exécuter VASP et les outils d'analyse.

Le TP se fera en ligne de commande via un terminal. Si vous vous êtes 
connecté à ``bilbo`` avec x2go, vous trouverez le terminal
dans le doc en bas de la fenêtre, ou via le menu Applications en haut
à gauche.

Utilisation des modules
=======================

Les modules d'environnement sont un moyen simple pour l'utilisateur de charger 
dans un terminal un environnement de travail spécifique à l'exécution ou 
l'utilisation de certaines ressources. C'est un outil que l'on retrouve sur la 
plupart des ressources de calcul HPC.

Pour afficher la liste des modules disponibles (module av est suffisant):

.. code-block:: console

    username@slurm-ens-frontal:~$ module avail
    ------------------------------------- /opt/cluster/modulefiles -------------------------------------
    crystal/14        gaussview/6.0  mkl/2023.0     python/3
    gaussian/g16-b01  gcc/9.1.0      openmp/latest  vasp/5.3.5

    --------------------------------- /opt/cluster/contrib/modulefiles ---------------------------------
    ams/2021.102  anvio/6.2         hysop/2.0.0       paraview/5.8.0-mpi  TP_gaussian/1  VESTA/3.5.7
    anaconda/3    avci/test         lammps/23Jun2022  pseudoVASP/PBE      TP_VASP/2022   vmd/1.9.3
    anvio/6.1     bioconda-tools/3  opencv/4.1.0      scalasca/2.6        turbomole/7.5

Pour charger (load) un module et donc l'environnement associé :

.. code-block:: console

    username@slurm-ens-frontal:~$ module load TP_VASP/2022
    Loading TP_VASP/2022
       Loading requirement: anaconda/3 python/3 pseudoVASP/PBE VESTA/3.5.7 vasp/5.3.5
    
Comme vous pouvez le voir, si le module nécessite le chargement d'autres modules, 
un message est affiché avec les modules chargés.

Chaque fois que vous ouvrez un nouveau terminal ou que vous vous reconnectez 
à bilbo vous devez charger les modules dont vous avez besoin.

Pour connaître la liste des modules actuellement chargés dans votre 
environnement de travail, utilisez la commande ``module list``

.. code-block:: console

    username@slurm-ens-frontal:~$ module list
    Currently Loaded Modulefiles:
    1) vasp/5.3.5   3) python/3         5) VESTA/3.5.7
    2) anaconda/3   4) pseudoVASP/PBE   6) TP_VASP/2022

Pour le TP, le module ``TP_VASP/2022`` charge tout ce dont vous avez besoin.
Si vous voulez juste exécuter VASP, il suffit de charger uniquement le
module ``vasp/5.3.5``.

Chaque module positionne des variables d'environnement permettant d'accéder 
plus facilement à certaines ressources ou d’exécuter certaines commandes.
Pour savoir ce que contient un module utilisez la commande ``module show``.

.. code-block:: console

    username@slurm-ens-frontal:~$ module show pseudoVASP/PBE
    -------------------------------------------------------------------
    /opt/cluster/contrib/modulefiles/pseudoVASP/PBE:

    module-whatis   {PBE pseudopotentials for VASP calculations}
    setenv          PAWROOT /opt/cluster/contrib/apps/pseudoVASP/potpaw_PBE
    -------------------------------------------------------------------

Le module ``pseudoVASP/PBE`` positionne une variable nommée ``PAWROOT``
contenant le chemin vers les pseudopotentiels PAW.

.. code-block:: console

    username@slurm-ens-frontal:~$ ls $PAWROOT
    Ac     B_s    Cu_pv      Gd_3   In     Mo     N_s    potcar.05Feb03.tar        Ru     Tb_3   V_pv
    Ac_s   C      data_base  Ge     In_d   Mo_pv  O      potcar_short.05Feb03.tar  Ru_pv  Tc     V_sv
    Ag     Ca_pv  Dy_3       Ge_d   Ir     N      O_h    potcar_short.tar          S      Tc_pv  W
    Al     Ca_sv  Er_2       Ge_h   K_pv   Na     O_s    potcar.tar                Sb     Te     W_pv
    ...


Vous pourrez vous servir de cette variable pour concatener des fichiers POTCAR.
Par exemple pour concatener les fichiers POTCAR de Ni et O pour le système NiO :

.. code-block:: console

    username@slurm-ens-frontal:~$ cat $PAWROOT/Ni/POTCAR $PAWROOT/O/POTCAR > POTCAR



Utilisation de slurm
====================

Les commandes listées ci-dessous sont liées à `slurm <https://slurm.schedmd.com/documentation.html>`_, 
le gestionnaire de travaux permettant de soumettre des calculs. L'objectif
d'un gestionnaire de travaux est de réserver les ressources nécessaires pour 
votre calcul et de gérer la file d'attente de l'ensemble des calculs soumis 
par tous les utilisateurs.

=========================  ======================================
Commande	               Description
=========================  ======================================
sbatch [script] [options]  Soumet un calcul en utilisant un script de soumission.
salloc --time=03:00:00     Permet de se connecter sur un nœud de calcul pour l'utilisation de ressources (par défaut 1 cpu), de façon interactive, pendant 3h.
squeue                     Affiche la liste des calculs en cours. squeue -u login affiche la liste des calculs du login.
scancel 7264               Stoppe le calcul numéro 7264. Le numéro est donné lors de la soumission ou par la commande squeue
=========================  ======================================

Mode interactif ``salloc``
--------------------------

Le mode interactif permet d'exécuter des programmes directement dans 
la ligne de commande, sans soumission à la queue de calculs.
Pour se faire, vous utiliserez la commande ``salloc``, par 
défaut cette commande réserve un cpu (un cœur) pendant une heure et ouvre 
une session interactive dans laquelle vous pourrez exécuter vos calculs. 
Pour que la session reste active plus longtemps, vous pouvez utiliser 
l'option ``--time``, permettant de spécifier un temps pour la session, par 
exemple pour 3h :

.. code-block:: console

    username@slurm-ens-frontal:~$ salloc --time=03:00:00
    salloc: Granted job allocation 6794
    salloc: Waiting for resource configuration
    salloc: Nodes slurm-ens1 are ready for job
    username@slurm-ens1:~$

Vous êtes maintenant sur un nœud de calcul (notez le ``slurm-ens1`` dans le
prompt) qui vous est réservé.
Après l'exécution de salloc vous devez charger les modules nécessaires.
Vous pouvez charger le module ``vasp/5.3.5`` et lancer le calcul.


Soumission en mode batch
------------------------

Le mode batch permet, à l'aide de commandes slurm, de soumettre un calcul 
depuis bilbo (le serveur frontal). De cette manière vous pouvez soumettre 
plusieurs calculs simultanément, chacun avec une durée et des ressources 
différentes (nombres de cpus, mémoires, temps ...).

Un script de soumission slurm est disponible avec le module ``vasp/5.3.5``.
On appelle habituellement cela un "job". Voici comment le récupérer.

.. code-block:: console

    username@slurm-ens-frontal:~$ module show TP_VASP/2022
    -------------------------------------------------------------------
    /opt/cluster/contrib/modulefiles/TP_VASP/2022:

    module-whatis   {Environnement pour un TP VASP}
    module          load vasp/5.3.5
    module          load python/3
    module          load pseudoVASP/PBE
    module          load VESTA
    prepend-path    PATH /opt/cluster/contrib/apps/TP_VASP/bin
    setenv          JOB /opt/cluster/contrib/apps/TP_VASP/job/vasp.job
    setenv          INPUTS /opt/cluster/contrib/apps/TP_VASP/inputs/
    -------------------------------------------------------------------
    username@slurm-ens-frontal:~$ ls $JOB
    /opt/cluster/contrib/apps/TP_VASP/job/vasp.job
    username@slurm-ens-frontal:~$ cp $JOB .

Le fichier vasp.job est utilisé par slurm pour (1) réserver les ressources 
informatiques et (2) exécuter le calcul demandé. Vous pouvez apporter des 
modifications à l'entête du fichier pour préciser le nombre de cœurs à 
utiliser, la durée estimée du calcul et la mémoire. 
Voici l'entête du fichier que vous pouvez modifier. 
Les lignes commençant par un ``#`` sont des commentaires et ne sont donc pas 
interprétées par le système. Les lignes commençant par ``#SBATCH`` sont celles 
utilisées par slurm et définissant le calcul.

.. code-block:: bash

    #!/bin/bash
    #
    #### JOB VASP ####
    #
    # NOM DU JOB
    ##SBATCH --job-name=run_vasp
    #
    # DUREE MAXIMALE DU JOB. Format : jours-heures:minutes:secondes
    #SBATCH --time=0-0:30:0
    #
    # RESSOURCES DEMANDEES
    #SBATCH --nodes=1
    #SBATCH --ntasks=1
    #SBATCH --cpus-per-task=1
    #

Une fois les ressources du calculs définies dans le fichier de soumission,
vous pouvez soumettre le calcul avec la commande ``sbatch``. Le fichier 
job doit alors se trouver dans le même dossier que les fichiers inputs 
de VASP : INCAR, POSCAR, POTCAR (et KPOINTS).

.. code-block:: console

    username@slurm-ens-frontal:~$ cp $JOB .
    username@slurm-ens-frontal:~$ ls
    INCAR  KPOINTS  POSCAR  POTCAR  vasp.job
    username@slurm-ens-frontal:~$ sbatch vasp.job
    Submitted batch job 38722
    username@slurm-ens-frontal:~$ squeue
    JOBID PARTITION     NAME     USER ST       TIME  NODES NODELIST(REASON)
    38722   compute vasp.job gvallver  R       0:09      1 slurm-ens1     

Comme vous pouvez le voir ci-dessus, la commande ``squeue`` vous permet
d'afficher la liste des travaux (jobs) actuellement en cours.

Configuration de python
=======================

Pour l'analyse des calculs, plusieurs scripts python sont utilisés. Pour
les utiliser il faut donc activer un environnement python dans lequel les
modules adéquats sont installés. Si vous avez chargé le module ``TP_VASP/2022``
le module python est chargé :

.. code-block:: console

    username@slurm-ens-frontal:~$ module load TP_VASP/2022
    Loading TP_VASP/2022
       Loading requirement: anaconda/3 python/3 pseudoVASP/PBE VESTA/3.5.7 vasp/5.3.5
    username@slurm-ens-frontal:~$ module list
    Currently Loaded Modulefiles:
     1) vasp/5.3.5   3) python/3         5) VESTA/3.5.7
     2) anaconda/3   4) pseudoVASP/PBE   6) TP_VASP/2022

Lors de votre première utilisation de bilbo, vous devez configurer votre 
environnement pour pouvoir activer / désactiver des environnements python.
Vous allez utiliser ``conda`` l'utilitaire en ligne de commande de la distribution
``anaconda``.

.. code-block:: console

    username@slurm-ens-frontal:~$ conda init
    username@slurm-ens-frontal:~$ source ~/.bashrc
    (base) username@slurm-ens-frontal:~$

La mention ``(base)`` doit apparaître devant votre prompt indiquant que vous 
utilisez actuellement l'environnement de base. Vous pouvez aussi fermer 
et réouvrir votre terminal. Les modules nécessaires sont
dans l'environnement ``pmg``, pour l'activer il faut saisir :

.. code-block:: console

    (base) username@slurm-ens-frontal:~$ which python
    /opt/cluster/contrib/apps/anaconda/3/bin/python
    (base) username@slurm-ens-frontal:~$ conda activate pmg
    (pmg) username@slurm-ens-frontal:~$ which python
    /opt/cluster/contrib/apps/anaconda/3/envs/pmg/bin/python
    (pmg) username@slurm-ens-frontal:~$ python
    Python 3.9.7 (default, Sep 16 2021, 13:09:58)
    [GCC 7.5.0] :: Anaconda, Inc. on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>>

Pour quitter l'environnement pmg il faut faire ``conda deactivate``.

Dans l'environnement pmg, vous pouvez utilisez les scripts python fournis
ou l'outil ``v``.


        

