.. TP VASP documentation master file, created by
   sphinx-quickstart on Tue Jan 15 16:57:05 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Travaux Pratiques/tutorial VASP
===============================

VASP ("Vienna Ab initio Simulation Package") est un logiciel de simulation des
propriétés électroniques de la matière condensée qui repose sur la théorie de la
fonctionnelle de la densité (DFT) et l'utilisation de conditions 3D-périodiques. Les
fonctions d'onde sont développées sur une base d'ondes planes et les électrons de coeur
sont représentés par des pseudopotentiels PAW.

Les différents calculs proposés ont en majorité une durée inférieure à une minute avec une
version séquentielle de VASP 
(exécuté sur un Dual-Core AMD Opteron(tm) Processor 2216 at 2.4 GHz, 1Mo cache, 8 Go RAM). 
Les calculs de DOS peuvent être un peu plus long (5 ou 10 min).
Dans chaque partie, une archive est disponnible contenant les fichiers inputs nécessaires
à la réalisation des calculs.

Liens utiles
------------
 
* Le site web du code : http://www.vasp.at/
* La documentation : https://www.vasp.at/wiki/index.php/The_VASP_Manual
* Wiki des mots clefs : https://www.vasp.at/wiki/index.php/Category:INCAR_tag
* présentation du workshop VASP : https://www.vasp.at/learn/
* Exemples de calculs : https://www.vasp.at/wiki/index.php/Category:Examples

Documents
---------

* Texte du TP en pdf : :download:`fichier pdf <doc/TP_VASP_RFCT.pdf>`
* Une présentation de VASP contenant et les résultats des cacluls du TP :
  :download:`presentation et correction <doc/tutorial_vasp.pdf>`.

Tables des matières
===================

.. toctree::
   :maxdepth: 2

   parts/index
   results/index
   tools/index

