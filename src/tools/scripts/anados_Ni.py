#!/usr/bin/env python
# -*-coding:utf-8 -*-

# load vasptools module
import vasptools
# load numpy module
import numpy as np

# extract DOS
calcul = vasptools.VaspRun()
calcul.lectureDOS()

# set the origin of energy to the fermi level
energie = [e - calcul.eFermi for e in calcul.energiesDOS]

# convert to numpy array for an easier treatment
dosProjetees = np.array(calcul.dosPartielles[0])
dosTotale = np.array(calcul.dosTotale)

# contribution of s AO
DOSs = dosProjetees[:,:,0]

# contribution of p AO
DOSp = dosProjetees[:,:,1] + dosProjetees[:,:,2] + dosProjetees[:,:,3]

# contribution of d AO
DOSd = dosProjetees[:,:,4] + dosProjetees[:,:,5] + dosProjetees[:,:,6] \
     + dosProjetees[:,:,7] + dosProjetees[:,:,8]

# print results
out  = "# column title\n"
out += "#  1 : E - Ef (eV)\n"
out += "#  2 : total DOS up\n" 
out += "#  3 : total DOS down\n"
out += "#  4 : s contribution to DOS up\n"
out += "#  5 : s contribution to DOS down\n"
out += "#  6 : p contribution to DOS up\n"
out += "#  7 : p contribution to DOS down\n"
out += "#  8 : d contribution to DOS up\n"
out += "#  9 : d contribution to DOS down\n"
for e, dosup, dosdown, sup, sdown, pup, pdown, dup, ddown in \
    zip(energie, dosTotale[0,:,0], dosTotale[1,:,0], DOSs[0], DOSs[1], DOSp[0], DOSp[1], DOSd[0], DOSd[1]):
    out += "%12.7f %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f\n" % \
        (e, dosup, -dosdown, sup, -sdown, pup, -pdown, dup, -ddown)
open("anaDOS.dat", "w").write(out)


