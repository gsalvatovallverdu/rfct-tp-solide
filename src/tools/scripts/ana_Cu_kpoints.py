#!/usr/bin/env python

# charge un module contenant un objet crystal
import crystal

# entete
print("#  k    a       b       c     alpha   beta  gamma     Econv           SinglePts        DE")

save = 0.0

# boucle sur les points k
for i in range(1, 13):

    # chargement de la structure
    struct = crystal.fromPOSCAR("CONTCAR_{0}".format(i), verbose = False)

    # lecture du fichier OSZICAR 
    oszicar = open("OSZICAR_{0}".format(i), "r").readlines()
    for line in oszicar:
        # lecture de la premiere energie SCF convergee
        # single point sur la structure experimentale
        if " 1 F=" in line:
            singlePoint = float(line.split()[4])

    # lecture de l'energie finale
    elast = float(oszicar[-1].split()[4])

    # affiche les resultats
    print("%4d %7.3f %7.3f %7.3f %6.1f %6.1f %6.1f %12.7f     %12.7f %12.3f" % (i, \
        struct.a, struct.b, struct.c, struct.alpha, struct.beta, struct.gamma, \
        elast, singlePoint, (singlePoint - save)*1000.))

    # sauvegarde l'energie de la structure exprimentale pour calculer
    # la difference
    save = singlePoint



