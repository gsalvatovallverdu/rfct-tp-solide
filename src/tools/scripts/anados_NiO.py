#!/usr/bin/env python
# -*-coding:utf-8 -*-

# load vasptools module
import vasptools
# load numpy module
import numpy as np

# extract DOS
calcul = vasptools.VaspRun()
calcul.lectureDOS()

# set the origin of energy to the fermi level
energie = [e - calcul.eFermi for e in calcul.energiesDOS]

# convert to numpy array for an easier treatment
dosProjetees = np.array(calcul.dosPartielles)
dosTotale = np.array(calcul.dosTotale)

# contribution of Ni atoms
dosNi = dosProjetees[0,:,:,:] + dosProjetees[1,:,:,:]

# contribution of O atoms
dosO  = dosProjetees[2,:,:,:] + dosProjetees[3,:,:,:]

out  = "# column title\n"
out += "#  1 : E - Ef (eV)\n"
out += "#  2 : total DOS up\n" 
out += "#  3 : total DOS down\n"
out += "#  4 : Ni s contribution to DOS up\n"
out += "#  5 : Ni s contribution to DOS down\n"
out += "#  6 : Ni p contribution to DOS up\n"
out += "#  7 : Ni p contribution to DOS down\n"
out += "#  8 : Ni d contribution to DOS up\n"
out += "#  9 : Ni d contribution to DOS down\n"
out += "# 10 :  O s contribution to DOS up\n"
out += "# 11 :  O s contribution to DOS down\n"
out += "# 12 :  O p contribution to DOS up\n"
out += "# 13 :  O p contribution to DOS down\n"
out += "# 14 :  O d contribution to DOS up\n"
out += "# 15 :  O d contribution to DOS down\n"
for ene, dosup, dosdown, dosNiup, dosNidown, dosOup, dosOdown in \
    zip(energie, dosTotale[0,:,0], dosTotale[1,:,0], dosNi[0,:,:], dosNi[1,:,:], dosO[0,:,:], dosO[1,:,:]):
    # sum Ni AO
    Ni_s_up = dosNiup[0]
    Ni_p_up = dosNiup[1] + dosNiup[2] + dosNiup[3]
    Ni_d_up = dosNiup[4] + dosNiup[5] + dosNiup[6] + dosNiup[7] + dosNiup[8]

    Ni_s_down = -(dosNidown[0])
    Ni_p_down = -(dosNidown[1] + dosNidown[2] + dosNidown[3])
    Ni_d_down = -(dosNidown[4] + dosNidown[5] + dosNidown[6] + dosNidown[7] + dosNidown[8])

    # sum O AO
    O_s_up = dosOup[0]
    O_p_up = dosOup[1] + dosOup[2] + dosOup[3]
    O_d_up = dosOup[4] + dosOup[5] + dosOup[6] + dosOup[7] + dosOup[8]

    O_s_down = -(dosOdown[0])
    O_p_down = -(dosOdown[1] + dosOdown[2] + dosOdown[3])
    O_d_down = -(dosOdown[4] + dosOdown[5] + dosOdown[6] + dosOdown[7] + dosOdown[8])

    # print all
    out += "%12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f %12.6f\n" % \
        (ene, dosup, -dosdown, Ni_s_up, Ni_s_down, \
                               Ni_p_up, Ni_p_down, \
                               Ni_d_up, Ni_d_down, \
                                O_s_up,  O_s_down, \
                                O_p_up,  O_p_down, \
                                O_d_up,  O_d_down)

open("anaDOS.dat", "w").write(out)

out  = "# column title\n"
out += "#  1 : E - Ef (eV)\n"
out += "#  2 : total DOS up\n" 
out += "#  3 : total DOS down\n"
out += "#  4 : t2g contribution to DOS up\n"
out += "#  5 : t2g contribution to DOS down\n"
out += "#  6 : eg  contribution to DOS up\n"
out += "#  7 : eg  contribution to DOS down\n"
for ene, dosup, dosdown, dosNiup, dosNidown in \
    zip(energie, dosTotale[0,:,0], dosTotale[1,:,0], dosNi[0,:,:], dosNi[1,:,:]):

    t2g_up   = dosNiup[4] + dosNiup[5] + dosNiup[7]
    t2g_down = -(dosNidown[4] + dosNidown[5] + dosNidown[7])

    eg_up   = dosNiup[6] + dosNiup[8]
    eg_down = -(dosNidown[6] + dosNidown[8])


    out += "%12.7f %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f\n" \
        % (ene, dosup, -dosdown, t2g_up, t2g_down, eg_up, eg_down)
open("t2g_eg.dat", "w").write(out)


