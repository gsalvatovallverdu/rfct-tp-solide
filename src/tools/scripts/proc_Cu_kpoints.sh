#!/bin/bash

# boucle sur i = 1 a 12
for i in $(seq 1 12)
    do
        # ecriture du fichier KPOINTS
        echo "k points"  >  KPOINTS
        echo " 0"        >> KPOINTS
        echo "Gamma"     >> KPOINTS
        echo " $i $i $i" >> KPOINTS
        echo " 0 0 0"    >> KPOINTS

        # affiche le fichier KPOINTS
        echo -e "\n ================================================================"
        echo " KPOINTS file"
        echo  " ================================================================"
        cat KPOINTS
        echo -e " ================================================================\n"

        # execution de vasp
        time mpiexec vasp

        # sauvegarde des fichiers importants
        echo ""
        cp -v OSZICAR OSZICAR_$i
        cp -v CONTCAR CONTCAR_$i

        # supprime les fichiers de fonction d'onde (WAVECAR)
        # et de densite (CHGCAR) pour eviter un restart
        echo ""
        rm -v WAVECAR CHGCAR

    done

