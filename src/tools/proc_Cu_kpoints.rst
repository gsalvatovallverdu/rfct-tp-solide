==================================================
Automatic calculations over several k-points grids
==================================================

Simple bash procedure
---------------------

The following bash script allows to run a series of calculations with 
increasing k-points grid from 1x1x1 to 12x12x12.

:download:`Download <scripts/proc_Cu_kpoints.sh>`.

.. literalinclude:: scripts/proc_Cu_kpoints.sh
    :language: bash
    :linenos:
    :caption: bash script proc.sh
    :name: proc_kpoints


Slurm procedure using KOINTS grids
----------------------------------

The following script includes the above loop in a slurm bash script to run
it in batch mode. The script automaticaly writes the INCAR file. The POSCAR
and POTCAR files must exist in the same folder.

.. literalinclude:: scripts/kpoints.job
    :language: bash
    :linenos:
    :caption: slurm script for k-points grid optimization

Slurm procedure using KSPACING keyword
--------------------------------------

The following script use the KSPACING keyword in INCAR instead of writing
grids in KPOINT files. The script automaticaly writes the INCAR file. The POSCAR
and POTCAR files must exist in the same folder.

.. literalinclude:: scripts/kpoints_kspacing.job
    :language: bash
    :linenos:
    :caption: slurm script for k-points grid optimization
