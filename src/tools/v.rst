========
v script
========

The script ``v`` is available if you load the ``TP_VASP/2022`` module.
This script provides a set of commands to get information from a VASP
calculation. Hereafter is the documentation of the script. The script 
is available here: https://github.com/gVallverdu/myScripts/tree/master/VASP 

.. code-block:: text

    Manage VASP job operation and extract interesting data.

    This script does not run the calculation. You have to check the input files
    before !!

    SYNTAX
            v <sub-command> [OPTIONS]

    SUB-COMMANDS

    get help
    --------

        -h, --hep, h, help
            print this help and exit.

    Manage calculations
    -------------------

        clean
            delete vasp output files in the current directory.

        restart [dirname]
            copy necessary files for a restart calculation into a restart
            directory named [dirname]. None of the copied files are modified.
            By default, the dirname is 'restart'.

        lr
            copy and save necessary files for restarting the calculation in the
            current directory. OSZICAR, OUTCAR, POSCAR and vasprun.xml files
            are saved.

        sp
            copy the necessary files for an energy calculation into a
            SinglePoint directory. In the new INCAR file, the following
            keywords are changed:
                * NSW    -> NSW = 0
                * ISMEAR -> ISMEAR = -5
                * EDIFFG -> deleted
                * IBRION -> deleted
                * ISIF   -> deleted
                * LREAL  -> deleted
                * SIGMA  -> deleted

        spb
            Do the same things as sp but add keywords for a Bader calculation:
                * LAECHG = True
                * NGXF   = XXX
                * NGYF   = YYY
                * NGZF   = ZZZ

            The values of XXX, YYY and ZZZ are computed from lattice parameters
            assuming a distance between grid points of 0.03A.

        purge
            *WARNING*, delete CHG, CHGCAR, WAVECAR, AECCAR* files in current
            directory and all subdirectories. Ask for authorization before
            deletion.

        makekpts
            Set up a KPOINTS file for bands structure calculations following
            Setyawan, W.; Curtarolo, S. Computational Materials Science 2010,
            49 (2), 299–312.

    Extract data
    ------------

        cell [FILENAME]
            print lattice parameters calculated from [FILENAME]. [FILENAME]
            must be a POSCAR/CONTCAR file. If [FILENAME] is absent, a list of
            available POSCAR/CONTCAR files in the working directory is printed.

        cl [OPTIONS]
            Read core state eigenenergies from OUTCAR file (need pymatgen).

            v cl
                Read the INCAR file and output core state eigenergy of the AO
                of the atom define by CLNT, CLN and CLL keywords.

            v cl select
                Output all available core state eigenergies and ask for the
                atom index and the AO name of the core state.

            c cl [atom index] [atomic orbital]
                Output the core state eigenergies of the atom and the atomic
                orbital defined in the options. The first atom index is 1.
                [atomic orbital] is something like 1s, 2p, 3s ...

        tdos ['FILENAME', tofile]
            read DOS from 'vasprun.xml' or from 'FILENAME' and plot it using
            matplotlib (need pymatgen). 'FILENAME' must be a valid vasprun.xml
            file.
            If tofile is present, the total DOS is printed into a file named
            'total_DOS.dat'.

        pdos ['FILENAME', tofile]
            read projected DOS from 'vasprun.xml' or from 'FILENAME' and plot
            it using matplotlib (need pymatgen). 'FILENAME' must be a valid
            vasprun.xml file.
            If tofile is present, for each element, projected DOS are printed
            into a file named 'DOS_X.dat' where X is the symbol of the
            element.

        atdos iat ['FILENAME']
            read projected DOS from 'vasprun.xml' or from 'FILENAME', extract
            projected DOS on atom index iat and dump the data into a file.
            iat goes from 1 to the number of atom.

        bands BANDPATH [DOSPATH]
            read band structure from 'vasprun.xml' and plot it using the
            BSPlotter tools included into pymatgen. BANDPATH and DOSPATH
            are the paths to the vasprun.xml file of the band structure
            calculation and the DOS calculation respectively. If DOSPATH is not
            present, the fermi level is taken from the band structure
            calculation (less accurate).

            Examples:
                v bands ./Bands ./DOS
                v bands ./

        mag [full, threshold]
            read OUTCAR and POSCAR file and print magnetization of each atom at
            the last ionic step.
            If 'full' is present, magnetization are plotted and printed into a
            file for all ionic step but only for atoms whit a magnetic moment
            higher than the treshold.o

            Examples:
                v mag
                v full
                v full .2

        charges [FILENAME]
            After a bader calculation
            `http://http://theory.cm.utexas.edu/henkelman/code/bader/`_
            Read atomic populations into ACF.dat file and compute atomic
            charges using data in POTCAR file and atom names in POSCAR file.
            If [FILENAME] is given, charges are printed into a file of name
            [FILENAME].

        bmag [FILENAME]
            After a bader calculation on an up - down density (spin density)
            `http://http://theory.cm.utexas.edu/henkelman/code/bader/`_
            Read atomic populations into ACF.dat file that correspond to the
            atomic magnetic moments number of up minus number of down electrons.
            If [FILENAME] is given, charges are printed into a file of name
            [FILENAME]

        cvg [STEP]
        cvg [OUTCAR] [OSZICAR] [POSCAR]
            Look at convergence of the calculation. Without any arguments, OUTCAR
            OSZICAR and POSCAR files are read. If [STEP] is present as un integer,
            considering the 'local restart' option, OUTCAR_step, OSZICAR_step and
            CONTCAR_step.vasp are read. Else, you can provide the name of OUTCAR,
            OSZICAR and POSCAR file in that order.

        forces ['FILENAME']
            Read forces acting on eahc ion in file 'FILENAME' and print them.
            Default is file 'OUTCAR', 'FILENAME' must be an OUTCAR file.

    Compute data
    ------------

        chgsum CHGCAR1 CHGCAR2 factor
            Compute CHGCAR1 + factor * CHGCAR2 where CHGCAR are denisty file.
            This command needs pymatgen.

        dep [FILE1, FILE2]
            Compute displacements of all atoms in two structures. By default,
            the vasprun.xml file is read and initial and final structures are
            compared. Two POSCAR/CONTCAR file can be also given in arguements.

        supercell NX NY NZ
            Build a supercell with the dimension NX*a x NY*b x NZ*c and ouput
            it in a POSCAR file.

            Example:
                v supercell 2 3 1

        neighbors iat radius [FILE]
            Print the neighbor list of atom [iat] in a sphere of radius
            [radius]. The structure is read on [FILE] (defautl value is
            'POSCAR'). The radius is in angstrom.

            Example:
                v neighbors 2 3.5

        allneighbors radius [FILE]
            Print all neighbors of all atoms in a sphere of radius [radius].
            The structure is read on [FILE]. The radius is in angstrom.

    REMARK
        Job files are supposed to begin by 'j' or to have  a '.job' extension.

        Some functions may need pymatgen module (see http://www.pymatgen.org).

