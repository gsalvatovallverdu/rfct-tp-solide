====================================
Analysis over several k-points grids
====================================

This page provdes python scripts using the `pymatgen <pymatgen.org>`_ 
python package to analyze the results. ``pymatgen`` is available in the
``pmg`` python environment. 

The scrit assume that the calculations were done using the bash procedure 
for the automatic calculations over a series of k-points grids:
:ref:`script proc.sh <proc_kpoints>`.

The output is a table with the optimized lattice parameters, the energy
of the optimized structure (``Econv``) in eV, the energy of the input
structure (``SinglePts``) in eV and the ``SinglePts`` energy difference between the 
given line and the previous one, in meV.

There is two version of the script. The first one assume the utilization 
of a k-points grid defined in the KPOINTS file. The second one assume 
the utilization of the KSPACING keywords in the INCAR file.

k-points grids in KPOINTS file
------------------------------

:download:`Téléchargement du script <scripts/ana_kpoints.py>`.

.. literalinclude:: scripts/ana_kpoints.py
    :language: python
    :linenos:
    :caption: loop over calculations done on several k-points grid defined in KPOINTS file
    :name: ana_Cu_kpoints

k-points grids defined from KSPACING
------------------------------------

:download:`Téléchargement du script <scripts/ana_kpoints.py>`.

.. literalinclude:: scripts/ana_kpoints_kspacing.py
    :language: python
    :linenos:
    :caption: loop over calculations done on several k-points grid defined with KSPACING

