==============
Scripts utiles
==============

Scripts utiles pour l'analyse des résultats ou la préparation des calculs

Au cours du TP, vous pourrez utiliser les scripts ci-dessous pour analyser les DOS ou
préparer les calculs. Ils sont présents dans les archives contenant les fichiers inputs
les différentes partie du TP. 

.. toctree::
   :maxdepth: 1

   proc_Cu_kpoints
   ana_Cu_kpoints
   v

Les scripts d'analyse des DOS ainsi que les commandes utilisées pour tracer les DOS ou le
diagramme de bandes utilisent la librairie python `pymatgen <pymatgen.org>`_.


